@echo off
TITLE [LOAD] (%cd%)
SET /A ARGS_COUNT=0
SET adbcommand=adb
FOR %%A in (%*) DO SET /A ARGS_COUNT+=1
if %ARGS_COUNT% == 1 SET adbcommand=%adbcommand% -s %1
echo %adbcommand%
@echo on
%adbcommand% reboot bootloader
pause
fastboot flash boot sdxprairie-boot.img
fastboot flash abl abl.elf
fastboot erase system
fastboot flash system sdxprairie-sysfs.ubi
pause
fastboot reboot