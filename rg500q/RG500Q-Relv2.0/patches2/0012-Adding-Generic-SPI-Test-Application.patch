From aa4751b3589cfdb6cd4700ac0f59b2deea861931 Mon Sep 17 00:00:00 2001
From: dev <dev@RG500Q_SDK>
Date: Tue, 25 May 2021 19:00:46 +0530
Subject: [PATCH 12/16] Adding Generic SPI Test Application

---
 sample/spi2/Makefile      |  26 +++
 sample/spi2/example_spi.c | 515 ++++++++++++++++++++++++++++++++++++++++++++++
 2 files changed, 541 insertions(+)
 create mode 100755 sample/spi2/Makefile
 create mode 100644 sample/spi2/example_spi.c

diff --git a/sample/spi2/Makefile b/sample/spi2/Makefile
new file mode 100755
index 0000000..85000be
--- /dev/null
+++ b/sample/spi2/Makefile
@@ -0,0 +1,26 @@
+CURR_DIR := $(shell pwd)
+
+-include ../../sdk.mk
+
+QL_TARGET_EXE = spi_test2
+QL_TARGET_OBJS = example_spi.o
+
+
+CFLAGS := $(QL_SDK_CFLAGS) $(QL_SDK_HARD_CFLAGS)
+LDFLAGS := $(QL_SDK_LDFLAGS) $(QL_SDK_LIBS)
+
+all: $(QL_TARGET_EXE)
+
+%.o:%.c
+	$(CC) -o $@ -c $^ $(CFLAGS)
+
+$(QL_TARGET_EXE): $(QL_TARGET_OBJS)
+	$(CC) -o $@ $(QL_TARGET_OBJS) $(LDFLAGS)
+
+	@echo "Copy "$(QL_TARGET_EXE)" to "$(QL_ROOTFS_BIN_DIR)
+	@cp $(QL_TARGET_EXE) $(QL_ROOTFS_BIN_DIR)
+	@cp $(QL_TARGET_EXE) $(QL_TARGET_OUT_DIR)
+
+clean:
+	rm -rf *.o
+	rm -rf $(QL_TARGET_EXE)
diff --git a/sample/spi2/example_spi.c b/sample/spi2/example_spi.c
new file mode 100644
index 0000000..5f91a05
--- /dev/null
+++ b/sample/spi2/example_spi.c
@@ -0,0 +1,515 @@
+// SPDX-License-Identifier: GPL-2.0-only
+/*
+ * SPI testing utility (using spidev driver)
+ *
+ * Copyright (c) 2020  Jethin Sekhar R <jethinr@qualcomm.com>
+ * Copyright (c) 2007  MontaVista Software, Inc.
+ * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
+ *
+ * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
+ */
+
+#include <stdint.h>
+#include <unistd.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <string.h>
+#include <errno.h>
+#include <getopt.h>
+#include <fcntl.h>
+#include <time.h>
+#include <sys/ioctl.h>
+#include <linux/ioctl.h>
+#include <sys/stat.h>
+#include <linux/types.h>
+#include <linux/spi/spidev.h>
+
+#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
+
+static void pabort(const char *s)
+{
+	if (errno != 0)
+		perror(s);
+	else
+		printf("%s\n", s);
+
+	abort();
+}
+
+static const char *device = "/dev/spidev1.0";
+static uint32_t mode;
+static uint8_t bits = 8;
+static char *input_file;
+static char *output_file;
+static uint32_t speed = 960000;
+static uint16_t delay;
+static int verbose;
+static int transfer_size;
+static int iterations;
+static int interval = 5; /* interval in seconds for showing transfer rate */
+
+uint8_t default_tx[] = {
+	0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
+	0x07, 0x08, 0x09, 0x10, 0x11, 0x12,
+	0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
+	0x19, 0x20, 0x21, 0x22, 0x23, 0x24,
+	0x25, 0x26, 0x27, 0x28, 0x29, 0x30,
+	0x31, 0x32,
+};
+
+uint8_t default_rx[ARRAY_SIZE(default_tx)] = {0, };
+uint32_t default_tx_size = sizeof(default_tx);
+char *input_tx;
+
+static void hex_dump(const void *src, size_t length, size_t line_size,
+		     char *prefix)
+{
+	int i = 0;
+	const unsigned char *address = src;
+	const unsigned char *line = address;
+	unsigned char c;
+
+	printf("%s | ", prefix);
+	while (length-- > 0) {
+		printf("%02X ", *address++);
+		if (!(++i % line_size) || (length == 0 && i % line_size)) {
+			if (length == 0) {
+				while (i++ % line_size)
+					printf("__ ");
+			}
+			printf(" |");
+			while (line < address) {
+				c = *line++;
+				printf("%c", (c < 32 || c > 126) ? '.' : c);
+			}
+			printf("|\n");
+			if (length > 0)
+				printf("%s | ", prefix);
+		}
+	}
+}
+
+/*
+ *  Unescape - process hexadecimal escape character
+ *      converts shell input "\x23" -> 0x23
+ */
+static int unescape(char *_dst, char *_src, size_t len)
+{
+	int ret = 0;
+	int match;
+	char *src = _src;
+	char *dst = _dst;
+	unsigned int ch;
+
+	while (*src) {
+		if (*src == '\\' && *(src+1) == 'x') {
+			match = sscanf(src + 2, "%2x", &ch);
+			if (!match)
+				pabort("malformed input string");
+
+			src += 4;
+			*dst++ = (unsigned char)ch;
+		} else {
+			*dst++ = *src++;
+		}
+		ret++;
+	}
+	return ret;
+}
+
+static void transfer(int fd, uint8_t const *tx, uint8_t const *rx, size_t len)
+{
+	int ret;
+	int out_fd;
+	struct spi_ioc_transfer tr = {
+		.tx_buf = (unsigned long)tx,
+		.rx_buf = (unsigned long)rx,
+		.len = len,
+		.delay_usecs = delay,
+		.speed_hz = speed,
+		.bits_per_word = bits,
+	};
+
+	if (mode & SPI_TX_QUAD)
+		tr.tx_nbits = 4;
+	else if (mode & SPI_TX_DUAL)
+		tr.tx_nbits = 2;
+	if (mode & SPI_RX_QUAD)
+		tr.rx_nbits = 4;
+	else if (mode & SPI_RX_DUAL)
+		tr.rx_nbits = 2;
+	if (!(mode & SPI_LOOP)) {
+		if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
+			tr.rx_buf = 0;
+		else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
+			tr.tx_buf = 0;
+	}
+
+	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
+	if (ret < 1)
+		pabort("can't send spi message");
+
+	if (verbose)
+		hex_dump(tx, len, 32, "TX");
+
+	if (output_file) {
+		out_fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0666);
+		if (out_fd < 0)
+			pabort("could not open output file");
+
+		ret = write(out_fd, rx, len);
+		if (ret != len)
+			pabort("not all bytes written to output file");
+
+		close(out_fd);
+	}
+
+	hex_dump(rx, len, 32, "RX");
+	hex_dump(tx, len, 32, "TX");
+}
+
+/**
+ Print_usage - This function is used to print the default usage available with the program.
+*/
+static void print_usage(const char *prog)
+{
+	printf("Usage: %s [-DsHOLCvpNtIh]\n", prog);
+	puts("  -D --device   device to use (default /dev/spidev1.0)\n"
+	     "  -s --speed    speed (Hz) (default 960000Hz)\n"
+	     "  -l --loop     enable loopback\n"
+	     "  -P --cpha     clock phase 0/1 (default 0)\n"
+	     "  -O --cpol     clock polarity 0/1 (default 0)\n"
+	     "  -C --cs-high  chip select active high\n"
+	     "  -g --string   Send data (e.g. \"1234\\xde\\xad\")\n"
+	     "  -t --data     Data buffer to fill the tx data buffer ex: -t 0xAA 0xBB 0xCC 0xDD)\n"
+	     "  -S --size     Send Random Data over SPI\n"
+	     "  -I --iter     iterations combine with size operation\n"
+	     "  -i --input    send content of a file\n"
+	     "  -o --output   recevied content to a file\n"
+	     "  -v --verbose  Verbose (show tx buffer)\n"
+	     "  -h --help     print this menu\n");
+	exit(1);
+}
+/**
+  This is the parser for the options in the command line
+*/
+static void parse_opts(int argc, char *argv[])
+{
+	while (1) {
+		static const struct option lopts[] = {
+			{ "device",  1, 0, 'D' },
+			{ "speed",   1, 0, 's' },
+			{ "delay",   1, 0, 'd' },
+			{ "bpw",     1, 0, 'b' },
+			{ "string",  1, 0, 'g' },
+			{ "data",    1, 0, 't' },
+			{ "input",   1, 0, 'i' },
+			{ "output",  1, 0, 'o' },
+			{ "loop",    0, 0, 'l' },
+			{ "cpha",    1, 0, 'P' },
+			{ "cpol",    1, 0, 'O' },
+			{ "lsb",     0, 0, 'L' },
+			{ "cs-high", 0, 0, 'C' },
+			{ "3wire",   0, 0, '3' },
+			{ "no-cs",   0, 0, 'N' },
+			{ "ready",   0, 0, 'R' },
+			{ "dual",    0, 0, '2' },
+			{ "verbose", 0, 0, 'v' },
+			{ "quad",    0, 0, '4' },
+			{ "size",    1, 0, 'S' },
+			{ "iter",    1, 0, 'I' },
+			{ "help",    0, 0, 'h' },
+			{ NULL, 0, 0, 0 },
+		};
+		int c;
+		int omode = 0;
+		int hmode = 0;
+		int index = 0;
+		c = getopt_long(argc, argv, "D:s:d:b:g:t:i:o:lP:O:LC3NR2v4S:I:h",
+				lopts, NULL);
+
+		if (c == -1)
+			break;
+
+		switch (c) {
+		case 'D':
+			device = optarg;
+			break;
+		case 's':
+			speed = atoi(optarg);
+			break;
+		case 'd':
+			delay = atoi(optarg);
+			break;
+		case 'b':
+			bits = atoi(optarg);
+			break;
+		case 'i':
+			input_file = optarg;
+			break;
+		case 'o':
+			output_file = optarg;
+			break;
+		case 'l':
+			mode |= SPI_LOOP;
+			break;
+		case 'P':
+			hmode = atoi(optarg);
+			if (hmode)
+				mode |= SPI_CPHA;
+			break;
+		case 'O':
+			omode = atoi(optarg);
+			if (omode)
+				mode |= SPI_CPOL;
+			break;
+		case 'L':
+			mode |= SPI_LSB_FIRST;
+			break;
+		case 'C':
+			mode |= SPI_CS_HIGH;
+			break;
+		case '3':
+			mode |= SPI_3WIRE;
+			break;
+		case 'N':
+			mode |= SPI_NO_CS;
+			break;
+		case 'v':
+			verbose = 1;
+			break;
+		case 'R':
+			mode |= SPI_READY;
+			break;
+		case 'g':
+			input_tx = optarg;
+			break;
+		//similar to p we have -t for the byte level transfer
+		case 't':
+
+			optind--;
+			while (optind < argc) {
+				if (*argv[optind] != '-') {
+					default_tx[index++] = strtol(argv[optind], NULL, 0);
+					optind++;
+					if (index == ARRAY_SIZE(default_tx))
+						break;
+				} else {
+					break;
+				}
+			}
+
+			default_tx_size = index;
+		case '2':
+			mode |= SPI_TX_DUAL;
+			break;
+		case '4':
+			mode |= SPI_TX_QUAD;
+			break;
+		case 'S':
+			transfer_size = atoi(optarg);
+			break;
+		case 'I':
+			iterations = atoi(optarg);
+			break;
+		case 'h':
+		default:
+			print_usage(argv[0]);
+			break;
+		}
+	}
+	if (mode & SPI_LOOP) {
+		if (mode & SPI_TX_DUAL)
+			mode |= SPI_RX_DUAL;
+		if (mode & SPI_TX_QUAD)
+			mode |= SPI_RX_QUAD;
+	}
+}
+
+static void transfer_escaped_string(int fd, char *str)
+{
+	size_t size = strlen(str);
+	uint8_t *tx;
+	uint8_t *rx;
+
+	tx = malloc(size);
+	if (!tx)
+		pabort("can't allocate tx buffer");
+
+	rx = malloc(size);
+	if (!rx)
+		pabort("can't allocate rx buffer");
+
+	size = unescape((char *)tx, str, size);
+	transfer(fd, tx, rx, size);
+	free(rx);
+	free(tx);
+}
+
+static void transfer_file(int fd, char *filename)
+{
+	ssize_t bytes;
+	struct stat sb;
+	int tx_fd;
+	uint8_t *tx;
+	uint8_t *rx;
+
+	if (stat(filename, &sb) == -1)
+		pabort("can't stat input file");
+
+	tx_fd = open(filename, O_RDONLY);
+	if (tx_fd < 0)
+		pabort("can't open input file");
+
+	tx = malloc(sb.st_size);
+	if (!tx)
+		pabort("can't allocate tx buffer");
+
+	rx = malloc(sb.st_size);
+	if (!rx)
+		pabort("can't allocate rx buffer");
+
+	bytes = read(tx_fd, tx, sb.st_size);
+	if (bytes != sb.st_size)
+		pabort("failed to read input file");
+
+	transfer(fd, tx, rx, sb.st_size);
+	free(rx);
+	free(tx);
+	close(tx_fd);
+}
+
+static uint64_t _read_count;
+static uint64_t _write_count;
+
+static void show_transfer_rate(void)
+{
+	static uint64_t prev_read_count, prev_write_count;
+	double rx_rate, tx_rate;
+
+	rx_rate = ((_read_count - prev_read_count) * 8) / (interval*1000.0);
+	tx_rate = ((_write_count - prev_write_count) * 8) / (interval*1000.0);
+
+	printf("rate: tx %.1fkbps, rx %.1fkbps\n", rx_rate, tx_rate);
+
+	prev_read_count = _read_count;
+	prev_write_count = _write_count;
+}
+
+/**
+this
+*/
+static void transfer_buf(int fd, int len)
+{
+	uint8_t *tx;
+	uint8_t *rx;
+	int i;
+
+	tx = malloc(len);
+	if (!tx)
+		pabort("can't allocate tx buffer");
+	for (i = 0; i < len; i++)
+		tx[i] = random();
+
+	rx = malloc(len);
+	if (!rx)
+		pabort("can't allocate rx buffer");
+
+	transfer(fd, tx, rx, len);
+
+	_write_count += len;
+	_read_count += len;
+
+	if (mode & SPI_LOOP) {
+		if (memcmp(tx, rx, len)) {
+			fprintf(stderr, "transfer error !\n");
+			hex_dump(tx, len, 32, "TX");
+			hex_dump(rx, len, 32, "RX");
+			exit(1);
+		}
+	}
+
+	free(rx);
+	free(tx);
+}
+
+
+int main(int argc, char *argv[])
+{
+	int ret = 0;
+	int fd;
+	parse_opts(argc, argv);
+
+	if (input_tx && input_file)
+		pabort("only one of -p and --input may be selected");
+
+	fd = open(device, O_RDWR);
+	if (fd < 0)
+		pabort("can't open device");
+
+	/*
+	 * spi mode
+	 */
+	ret = ioctl(fd, SPI_IOC_WR_MODE32, &mode);
+	if (ret == -1)
+		pabort("can't set spi mode");
+
+	ret = ioctl(fd, SPI_IOC_RD_MODE32, &mode);
+	if (ret == -1)
+		pabort("can't get spi mode");
+
+	/*
+	 * bits per word
+	 */
+	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
+	if (ret == -1)
+		pabort("can't set bits per word");
+
+	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
+	if (ret == -1)
+		pabort("can't get bits per word");
+
+	/*
+	 * max speed hz
+	 */
+	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
+	if (ret == -1)
+		pabort("can't set max speed hz");
+
+	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
+	if (ret == -1)
+		pabort("can't get max speed hz");
+
+	printf("spi mode: 0x%x\n", mode);
+	printf("bits per word: %d\n", bits);
+	printf("speed: %d Hz (%d KHz)\n", speed, speed/1000);
+
+	if (input_tx)
+		transfer_escaped_string(fd, input_tx);
+	else if (input_file)
+		transfer_file(fd, input_file);
+	else if (transfer_size) {
+		struct timespec last_stat;
+
+		clock_gettime(CLOCK_MONOTONIC, &last_stat);
+
+		while (iterations-- > 0) {
+			struct timespec current;
+
+			transfer_buf(fd, transfer_size);
+
+			clock_gettime(CLOCK_MONOTONIC, &current);
+			if (current.tv_sec - last_stat.tv_sec > interval) {
+				show_transfer_rate();
+				last_stat = current;
+			}
+		}
+		printf("total: tx %.1fKB, rx %.1fKB\n",
+		       _write_count/1024.0, _read_count/1024.0);
+	} else
+		transfer(fd, default_tx, default_rx, default_tx_size);
+
+	close(fd);
+
+	return ret;
+}
-- 
2.7.4

