#/bin/bash

#===============================================================================#
#Variables and Config Parameters
#===============================================================================#
_config()
{
	WORKDIR=`pwd`
	BUILDNAME="RG500Q"
	MAKE_JOB_THREADS=4
	ENABLE_PROMTS=1
	PRINT_ONLY=0
	FORCE_DOWNLOAD=1
	USE_LOCAL=0
	RETAIN_OLD_BIN=0
	CHECK_POINT_VAL=8192
	NO_TOOL_CHAIN_UPDATE=0
	ERROR_SUCCESS=0
	ERROR_FAILURE=1
	ERROR_MIN_RETRY=5
	MAKE_SUCCESS=0
	MAKE_FAILURE=2
	SET_VALUE=1
	CLEAR_VALUE=0
	MENU_MAIN=0
	MENU_CONFIG=1
	GET_ITEM_DEFAULT=0
	GET_ITEM_BUILDCMD=1
	GET_ITEM_IMAGENAME=2
	GET_ITEM_IMAGENAME_MAKEALL=3
	GET_ITEM_LOG_SUCCESS=4
	GIT_USER_NAME="dev"
	GIT_USER_EMAIL="dev@$BUILDNAME"
	GIT_LOG_COUNT=15
	SDK_FILE="ql-ol-extsdk.tar.gz"
	SDK_DIR_ACTUAL="ql-ol-extsdk"
	SDK_DIR="Linux_SDK_RG500Q"
	TOOLCHAIN_FILE="RG500QEAAAR11A02M4G_OCPU_01.001.01.001-toolchain.tar.gz"
	TOOLCHAIN_INSTALL_PATH="/opt/ql_crosstools"
	ROOTFS_FILE="ql-ol-rootfs.tar.gz"
	ROOTFS_DIR_NAME="ql-ol-rootfs"
	LOCAL_REPO_FILE="Linux_SDK_RG500Q.tar.gz"
	OUTPUT_DIR_NAME="target"
	BIN_DIR_NAME="bin"
	FLASHING_SCRIPT="load.bat"
	SAMPLE_APPS_DIR_NAME="sample"
	CLO_REL_DIR_NAME="rg500q"
	CLO_REL_TARGET_DIR_NAME="rg500q"
	CLO_REL_VERSION="RG500Q-Relv2.0"
	CLO_REL_PATH="https://git.codelinaro.org/clo/startup-kits/patches.git"
	PATCH_DIR_NAME="patches"
	PATCH1_DIR_NAME_SUFFIX=""
	PATCH2_DIR_NAME_SUFFIX="2"
	PATCH3_DIR_NAME_SUFFIX="3"
	_config_package_list
	_config_backup_dir
	_config_binary_list
}

_reconfig()
{
	BUILDROOT="$WORKDIR/$BUILDNAME"
	SDK_ROOT="$BUILDROOT/$SDK_DIR"
	ROOTFS_DIR="$SDK_ROOT/$ROOTFS_DIR_NAME"
	OUTPUT_DIR="$SDK_ROOT/$OUTPUT_DIR_NAME"
	OUTPUT_TARGET_PATH="$OUTPUT_DIR"
	BIN_DIR="$SDK_ROOT/$BIN_DIR_NAME"
	SAMPLE_APPS_DIR="$SDK_ROOT/$SAMPLE_APPS_DIR_NAME"
	SAMPLE_BIN_DIR="$OUTPUT_TARGET_PATH/$SAMPLE_APPS_DIR_NAME"
	CLO_CLONE_DIR="$WORKDIR/$CLO_REL_DIR_NAME"
	CLO_REL_DIR="$CLO_CLONE_DIR/$CLO_REL_TARGET_DIR_NAME/$CLO_REL_VERSION"
	PATCH1_DIR_NAME="$PATCH_DIR_NAME$PATCH1_DIR_NAME_SUFFIX"
	PATCH2_DIR_NAME="$PATCH_DIR_NAME$PATCH2_DIR_NAME_SUFFIX"
	PATCH3_DIR_NAME="$PATCH_DIR_NAME$PATCH3_DIR_NAME_SUFFIX"
	PATCH_DIR1="$CLO_REL_DIR/$PATCH1_DIR_NAME"
	PATCH_DIR2="$CLO_REL_DIR/$PATCH2_DIR_NAME"
	PATCH_DIR3="$CLO_REL_DIR/$PATCH3_DIR_NAME"
}

_config_package_list()
{
	PACKAGE_LIST="\
		bc              \
		build-essential \
		git             \
		htop	        \
		python          \
		tree            \
		zip             \
		"
}

_config_backup_dir()
{
	BACKUP_DIR_CONFIG_FILE="BackupCfg.txt"
	BACKUP_DIR_CONFIG="        \
		./$OUTPUT_DIR_NAME     \
		.git/*                \
		"
}

_config_binary_list()
 {
	DEFAULT_LOG_SUCCESS_STRING=""
	DISABLE_LOG_VERIFY=""
	ABOOT_BUILD_TYPE="1_aboot"
	ABOOT_BUILD_CMD="aboot ABOOT=1"
	ABOOT_IMAGE_NAME="abl.elf"
	ABOOT_MAKE_ALL="1"
	ABOOT_MAKE_SUCCESS_STRING="$DISABLE_LOG_VERIFY"
	BOOT_BUILD_TYPE="2_boot"
	BOOT_BUILD_CMD="kernel"
	BOOT_IMAGE_NAME="sdxprairie-boot.img"
	BOOT_MAKE_ALL="1"
	BOOT_MAKE_SUCCESS_STRING="$DISABLE_LOG_VERIFY"
	SAMPLE_APPS_BUILD_TYPE="3_sampleapps"
	SAMPLE_APPS_BUILD_CMD=""
	SAMPLE_APPS_IMAGE_NAME="sample/"
	SAMPLE_APPS_MAKE_ALL="1"
	SAMPLE_APPS_MAKE_SUCCESS_STRING="$DISABLE_LOG_VERIFY"
	SYSTEM_BUILD_TYPE="4_system"
	SYSTEM_BUILD_CMD="rootfs"
	SYSTEM_IMAGE_NAME="sdxprairie-sysfs.ubi"
	SYSTEM_MAKE_ALL="1"
	SYSTEM_MAKE_SUCCESS_STRING="$DISABLE_LOG_VERIFY"
}

#===============================================================================#
# Display all Config Parameters
#===============================================================================#
print_config()
{
	echo "WORKDIR                 = "$WORKDIR
	echo "BUILDNAME               = "$BUILDNAME
	echo "MAKE_JOB_THREADS        = "$MAKE_JOB_THREADS
	echo "ENABLE_PROMTS           = "$ENABLE_PROMTS
	echo "PRINT_ONLY              = "$PRINT_ONLY
	echo "FORCE_DOWNLOAD          = "$FORCE_DOWNLOAD
	echo "USE_LOCAL               = "$USE_LOCAL
	echo "RETAIN_OLD_BIN          = "$RETAIN_OLD_BIN
	echo "NO_TOOL_CHAIN_UPDATE    = "$NO_TOOL_CHAIN_UPDATE
	echo "GIT_USER_NAME           = "$GIT_USER_NAME
	echo "GIT_USER_EMAIL          = "$GIT_USER_EMAIL
	echo "GIT_LOG_COUNT           = "$GIT_LOG_COUNT
	echo "BUILDROOT               = "$BUILDROOT
	echo "SDK_FILE                = "$SDK_FILE
	echo "SDK_DIR                 = "$SDK_DIR
	echo "SDK_DIR_ACTUAL          = "$SDK_DIR_ACTUAL
	echo "SDK_ROOT                = "$SDK_ROOT
	echo "TOOLCHAIN_INSTALL_PATH  = "$TOOLCHAIN_INSTALL_PATH
	echo "ROOTFS_FILE             = "$ROOTFS_FILE
	echo "ROOTFS_DIR_NAME         = "$ROOTFS_DIR_NAME
	echo "ROOTFS_DIR              = "$ROOTFS_DIR
	echo "LOCAL_REPO_FILE         = "$LOCAL_REPO_FILE
	echo "OUTPUT_DIR_NAME         = "$OUTPUT_DIR_NAME
	echo "OUTPUT_DIR              = "$OUTPUT_DIR
	echo "BIN_DIR_NAME            = "$BIN_DIR_NAME
	echo "BIN_DIR                 = "$BIN_DIR
	echo "OUTPUT_TARGET_DIR       = "$OUTPUT_TARGET_DIR
	echo "OUTPUT_TARGET_PATH      = "$OUTPUT_TARGET_PATH
	echo "SAMPLE_APPS_DIR_NAME    = "$SAMPLE_APPS_DIR_NAME
	echo "PATCH_DIR_NAME          = "$PATCH_DIR_NAME
	echo "SAMPLE_APPS_DIR         = "$SAMPLE_APPS_DIR
	echo "PATCH_DIR1              = "$PATCH_DIR1
	echo "PATCH_DIR2              = "$PATCH_DIR2
	echo "PATCH_DIR3              = "$PATCH_DIR3
	echo "CLO_REL_DIR_NAME        = "$CLO_REL_DIR_NAME
	echo "CLO_REL_TARGET_DIR_NAME = "$CLO_REL_TARGET_DIR_NAME
	echo "CLO_REL_DIR             = "$CLO_REL_DIR
	echo "CLO_CLONE_DIR           = "$CLO_CLONE_DIR
	echo "CLO_REL_VERSION         = "$CLO_REL_VERSION
	echo "CLO_REL_PATH            = "$CLO_REL_PATH
	echo "TOOLCHAIN_FILE          = "$TOOLCHAIN_FILE
	echo "PACKAGE_LIST            = "$PACKAGE_LIST
}

check_for_confirmation()
{
	msg=$@
	local confirm_to_continue
	confirm_to_continue=$ERROR_SUCCESS
	if [ "$ENABLE_PROMTS" = "$SET_VALUE" ]
	then
		while true; do
			echo $msg
			read -r -p "Enter Yes(Yy)/No(Nn)/Skip(Ss) :  " answer
			case $answer in
				[Yy]* ) break;;
				[Nn]* ) continue;;
				[Ss]* )
					confirm_to_continue=$ERROR_FAILURE
					break;;
				* ) echo "Please Answer Y to continue";;
			esac
		done
	fi
	return $confirm_to_continue;
}

check_file_copy_confirmation()
{
	msg="${@:3}"
	file_path=$1
	file_path_alt=$2
	if [ "$ENABLE_PROMTS" = "$SET_VALUE" ]
	then
		while true; do
			echo $msg
			read -r -p "Enter (Yy/Nn): " answer
			case $answer in
				[Yy]* )
				if [ ! -e $file_path ] ;
				then
					if [ -e $file_path_alt ]
					then
						cp $file_path_alt $file_path
						break;
					else
						echo -e "$RED ERROR! $file_path or $file_path_alt Not Found $ENDCOLOR";
						continue;
					fi
				else
					break;
				fi
				;;
				[Nn]* ) continue;;
				* ) echo "Please Answer Y to continue";;
			esac
		done
	else
		echo $msg
		if [ ! -e $file_path ]
		then
			if [ -e $file_path_alt ]
			then
				cp $file_path_alt $file_path
			else
				echo -e "$RED ERROR! $file_path or $file_path_alt Not Found $ENDCOLOR";
				exit;
			fi
		fi
	fi
}

check_for_sdk_exists()
{
	isprint=$1
	if [[ "$ENABLE_PROMTS" = "$SET_VALUE" ]] && [[ -z "$isprint" ]]
	then
		while true; do
			if [ ! -e $BUILDROOT ] ;
			then
				break;
			else
				echo "Build Already Exist, Continue with Rename(Y)/Replace(R) or Exit(N)"
				read -r -p "Enter (Yy/Nn/Rr): " answer
				case $answer in
					[Yy]* )
					update_build_name;
					continue;;
					[Rr]* )
					rm -rf $BUILDROOT;
					break;;
					[Nn]* )
					exit;;
				esac
			fi
		done
	else
		$isprint \
		rm -rf $BUILDROOT
	fi
}

check_for_clo_rel_exists()
{
	isprint=$1
	local do_download
	do_download=$ERROR_SUCCESS

	if [[ "$ENABLE_PROMTS" = "$SET_VALUE" ]] && [[ -z "$isprint" ]]
	then
		while true; do
			if [ ! -e $CLO_CLONE_DIR ] ;
			then
				break;
			else
				echo "CLO Release Already Exist, Continue with Rename(Y)/Replace(R)/UseExisting(N)"
				read -r -p "Enter (Yy/Nn/Rr): " answer
				case $answer in
					[Yy]* )
					update_clo_dir_name;
					continue;;
					[Rr]* )
					rm -rf $CLO_CLONE_DIR;
					break;;
					[Nn]* )
					do_download=$ERROR_FAILURE
					break;;
				esac
			fi
		done
	fi

	return $do_download;
}

# Instructions to Begin with
start_instructions()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ -z "$isprint" ]
	then
		msg="Read Confirmation Required"
		# check_for_confirmation $msg
	else
		echo -e $GREEN"This Script is used for demostrating the instructions\n"\
			"All Instructions used in the scripts are displayed in screen for future reference\n"\
			"Therefore the script is not mandatory\n"\
			$ENDCOLOR
	fi

	return $result
}

# Install Tools- Python and Virtual Env
install_tools()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	sudo add-apt-repository -y -u ppa:git-core/ppa

	$isprint \
	sudo apt update

	$isprint \
	sudo apt --assume-yes install $PACKAGE_LIST

	return $result
}

#Setup Root
setup_root()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	mkdir -p $PATCH_DIR1

	check_for_sdk_exists $isprint

	$isprint \
	mkdir -p $BUILDROOT

	$isprint \
	cd $BUILDROOT

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required $SDK_FILE to $PATCH_DIR1/$SDK_FILE or $WORKDIR/$SDK_FILE"
		check_file_copy_confirmation $PATCH_DIR1/$SDK_FILE $WORKDIR/$SDK_FILE $msg
	else
		echo -e $RED"MANDATORY! Manual Step \n"$GREEN\
			"Copy the SDK File ("$SDK_FILE") to \n"$PATCH_DIR1/$SDK_FILE "\n or\n"$WORKDIR/$SDK_FILE \
			"before proceeding to next steps"$ENDCOLOR
	fi

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required $TOOLCHAIN_FILE to $PATCH_DIR1/$TOOLCHAIN_FILE or $WORKDIR/$TOOLCHAIN_FILE"
		check_file_copy_confirmation $PATCH_DIR1/$TOOLCHAIN_FILE $WORKDIR/$TOOLCHAIN_FILE $msg
	else
		echo -e $RED"MANDATORY! Manual Step \n"$GREEN\
			"Copy the TOOLCHAIN File ("$TOOLCHAIN_FILE") to\n"$PATCH_DIR1/$TOOLCHAIN_FILE"\n or\n"$WORKDIR/$TOOLCHAIN_FILE \
			"before proceeding to next steps"$ENDCOLOR
	fi

	$isprint \
	ls -al $WORKDIR $PATCH_DIR1 $BUILDROOT

	return $result
}

#Setup Tool Chain
setup_tool_chain()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ "$NO_TOOL_CHAIN_UPDATE" = "$SET_VALUE" ]
	then
		return
	fi

	$isprint \
	cd $BUILDROOT

	$isprint \
	sudo rm -rf $TOOLCHAIN_INSTALL_PATH

	$isprint \
	sudo mkdir -p $TOOLCHAIN_INSTALL_PATH

	$isprint \
	sudo chmod 777 $TOOLCHAIN_INSTALL_PATH

	$isprint \
	tar --checkpoint=$CHECK_POINT_VAL -xf $PATCH_DIR1/$TOOLCHAIN_FILE -C $TOOLCHAIN_INSTALL_PATH

	$isprint \
	ls -al $TOOLCHAIN_INSTALL_PATH $TOOLCHAIN_INSTALL_PATH/*

	return $result
}

run_command()
{
	isprint=$1 #Get the Print Command
	local command_name=$2
	local command_to_run=$3
	local confirm_to_continue
	local run_success=$ERROR_FAILURE
	local retry_left=$ERROR_MIN_RETRY

	while [[ $run_success != $ERROR_SUCCESS ]]; do
		$isprint \
		$command_to_run
		run_success=$?

		if [ -z "$isprint" ]
		then

			case $run_success in
				[$ERROR_SUCCESS] )
					echo -e "$YELLOW $command_name Command SUCCESS!\n$command_to_run  $ENDCOLOR";;
				* )
					echo -e "$RED ERROR! $command_name Command FAILED! Retry ($retry_left retry left)\n $command_to_run  $ENDCOLOR";
					msg="Confirm for Retry"
					confirm_to_continue=$ERROR_SUCCESS
					wait_for_user $msg
					confirm_to_continue=$?
					if [[ "$confirm_to_continue" != "$ERROR_SUCCESS" ]]
					then
						break;
					fi
			esac
		fi

		retry_left=$(expr $retry_left - 1)
		if test $retry_left -lt 0
		then
			break;
		fi

	done

	return $run_success;
}

#Setup SDK
setup_sdk_repo()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	cd $BUILDROOT

	$isprint \
	tar --checkpoint=$CHECK_POINT_VAL -xf $PATCH_DIR1/$SDK_FILE

	$isprint \
	mv "$BUILDROOT/$SDK_DIR_ACTUAL" $SDK_ROOT

	$isprint \
	cd $SDK_ROOT

	$isprint \
	mkdir -p $ROOTFS_DIR

	$isprint \
	sudo tar -xzf $ROOTFS_FILE -C $ROOTFS_DIR

	$isprint \
	sudo chown -R $USER $ROOTFS_DIR

	$isprint \
	ls -al $BUILDROOT $SDK_ROOT $SDK_ROOT/*

	return $result
}

#Setup SDK
setup_sdk_local()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	mkdir -p $SDK_ROOT

	$isprint \
	cd $SDK_ROOT

	$isprint \
	tar --checkpoint=$CHECK_POINT_VAL -xvf $WORKDIR/$LOCAL_REPO_FILE -C $SDK_ROOT

	$isprint \
	ls -al $BUILDROOT $SDK_ROOT

	return $result
}

#Setup SDK
setup_sdk()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ $USE_LOCAL -ne 1 ]
	then
		setup_sdk_repo $isprint
		result=$?
	else
		setup_sdk_local $isprint
		result=$?
	fi

	return $result
}

display_git_status()
{
	isprint=$1 #Get the Print Command

	$isprint \
	git log --oneline -"$GIT_LOG_COUNT"

	$isprint \
	git status
}

#Setup git
setup_git()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	cd $WORKDIR

	$isprint \
	git config --global init.defaultBranch main

	$isprint \
	git config --global user.name $GIT_USER_NAME

	$isprint \
	git config --global user.email $GIT_USER_EMAIL

	$isprint \
	ls -al $WORKDIR

	return $result
}

#Initialize git for each project
initialize_git_folder()
{
	isprint=$1 #Get the Print Command
	PATCH_DIR_NAME=$2
	PATCH_SUB_DIR=$3
	PATCH_SOURCE_DIR=$CLO_REL_DIR/$PATCH_DIR_NAME
	PATCH_SOURCE_SUB_DIR=$PATCH_SOURCE_DIR/$PATCH_SUB_DIR
	PATCH_TARGET_DIR=$SDK_ROOT/$PATCH_SUB_DIR

	if [ -z "$isprint" ]
	then
		echo -e "$YELLOW  Performing InitialCommit on $PATCH_TARGET_DIR ... $ENDCOLOR"
	fi

	if [ ! -e $PATCH_TARGET_DIR ] ;
	then
		if [ -z "$isprint" ]
		then
			echo -e "$RED $PATCH_TARGET_DIR does not exist in SDK_ROOT:$SDK_ROOT $ENDCOLOR"
		fi

		$isprint \
		mkdir -p $PATCH_TARGET_DIR

	fi

	$isprint \
	cd $PATCH_TARGET_DIR

	$isprint \
	git init

	$isprint \
	git add .

	$isprint \
	git commit -m '"'"[$BUILDNAME Initial Commit] Project:$PATCH_SUB_DIR"'"' -s
}

#Applying the Patches
apply_patches()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS
	PATCH_DIR_NAME=$2
	PATCH_DIR=$CLO_REL_DIR/$PATCH_DIR_NAME

	cd $PATCH_DIR

	PATCH_TARGET_DIR_NAME=$(echo "$PATCH_DIR_NAME" | sed -r 's/\b[^/]+[/]//')

	android_patch_list=$( find $PATCH_DIR/* -type f -name '*.patch' | sed 's/^.*\/'$PATCH_TARGET_DIR_NAME'\(.*\)\/[^/]\+$/\1\/.\//' |sort -u)

	cd $SDK_ROOT

	for patch_dir in $android_patch_list; do
		patch_list=$(find $PATCH_DIR/$patch_dir -type f -name "*.patch" | sort) &&

		initialize_git_folder "$isprint" "$PATCH_DIR_NAME" "$patch_dir"
		for patch in $patch_list; do
			$isprint \
			git am $patch
		done
		display_git_status $isprint
	done

	return $result
}

setup_bin_dir()
{
	isprint=$1 #Get the Print Command

	if [ ! -e $BIN_DIR ] ;
	then
		$isprint \
		mkdir -p $BIN_DIR
	fi
}

#Setup Enviorment
setup_env()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $SDK_ROOT

	setup_bin_dir $isprint
}

get_build_item_details()
{
	isprint=$1
	build_type=$2
	return_item=$3
	local image_name=""
	local image_name_make_all=""
	local build_cmd=""
	local build_type_r=""
	local log_success_string="$DEFAULT_LOG_SUCCESS_STRING"

	declare -A build_cmd_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_BUILD_CMD
		[$BOOT_BUILD_TYPE]=$BOOT_BUILD_CMD
		[$SAMPLE_APPS_BUILD_TYPE]=$SAMPLE_APPS_BUILD_CMD
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_BUILD_CMD
	)

	declare -A image_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_IMAGE_NAME
		[$BOOT_BUILD_TYPE]=$BOOT_IMAGE_NAME
		[$SAMPLE_APPS_BUILD_TYPE]=$SAMPLE_APPS_IMAGE_NAME
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_IMAGE_NAME
	)

	declare -A make_all_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_MAKE_ALL
		[$BOOT_BUILD_TYPE]=$BOOT_MAKE_ALL
		[$SAMPLE_APPS_BUILD_TYPE]=$SAMPLE_APPS_MAKE_ALL
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_MAKE_ALL
	)

	declare -A log_str_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_MAKE_SUCCESS_STRING
		[$BOOT_BUILD_TYPE]=$BOOT_MAKE_SUCCESS_STRING
		[$SAMPLE_APPS_BUILD_TYPE]=$SAMPLE_APPS_MAKE_SUCCESS_STRING
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_MAKE_SUCCESS_STRING
	)

	build_type_list=$(echo "${!build_cmd_list[@]}" | tr ' ' $'\n' | sort -u)

	for type in $build_type_list; do
		if [ "$type" = "$build_type" ]; then
			build_cmd="${build_cmd_list[$type]}"
			image_name="${image_list[$type]}"
			log_success_string="${log_str_list[$type]}"
			build_type_r="$type"
			break;
		fi
		build_cmd+=" ${build_cmd_list[$type]}"
		image_name+=" ${image_list[$type]}"
		build_type_r+=" $type"
		if [ "$SET_VALUE" = "${make_all_list[$type]}" ]; then
			image_name_make_all+=" ${image_list[$type]}"
		fi
	done

	case $return_item in
		$GET_ITEM_BUILDCMD)
			echo $build_cmd;;
		$GET_ITEM_IMAGENAME)
			echo $image_name;;
		$GET_ITEM_IMAGENAME_MAKEALL)
			echo $image_name_make_all;;
		$GET_ITEM_LOG_SUCCESS)
			echo $log_success_string;;
		*)
			echo $build_type_r;;
	esac
}

make_images()
{
	isprint=$1 #Get the Print Command
	build_type=$2 #Get the Build Command
	local build_result=$MAKE_SUCCESS
	local build_command=""

	if [ ! -z "$build_type" ]
	then
		build_command=$(get_build_item_details "" "$build_type" $GET_ITEM_BUILDCMD)
	fi

	setup_env $isprint

	if [ "$build_type" = "$SAMPLE_APPS_BUILD_TYPE" ]
	then
		$isprint \
		cd $SAMPLE_APPS_DIR

		$isprint \
		mkdir -p $SAMPLE_BIN_DIR
	else
		$isprint \
		cd $SDK_ROOT
	fi

	BUILD_LOG_FILE_NAME="$BUILDNAME""$build_type""_"$(date +"%y%m%d_%H%M%S").log
	make_command="make $build_command"
	make_command+=" -j$MAKE_JOB_THREADS 2>&1"
	log_command="tee $BUILDROOT/$BUILD_LOG_FILE_NAME"
	make_command+=" | $log_command"
	make_command_no_result="$make_command"
	make_command+='; build_result=${PIPESTATUS[0]}'
	if [ -z "$isprint" ]
	then
		eval $make_command
	else
		echo $make_command_no_result
	fi

	if [ ! -z "$build_type" ] && [[ $build_result = $MAKE_SUCCESS ]]
	then
		#Copy the Images Here
		image_name=$(get_build_item_details "" "$build_type" $GET_ITEM_IMAGENAME)
		$isprint \
		cp -rf $OUTPUT_TARGET_PATH/$image_name $BIN_DIR
	fi

	$isprint \
	ls -al $BIN_DIR

	return $build_result
}

# #Build All Images
build_all()
{
	isprint=$1 #Get the Print Command
	local build_success=$MAKE_FAILURE
	local result=$ERROR_SUCCESS

	build_type_list=$(get_build_item_details "" "" $GET_ITEM_DEFAULT)

	for build_type in $build_type_list; do
		build_and_verify "$isprint" "$build_type"
		build_success=$?
		if [[ $build_success != $MAKE_SUCCESS ]]
		then
			result=$ERROR_FAILURE
			break
		fi
	done

	return $result
}

#Lookup all images
image_lookup()
{
	isprint=$1 #Get the Print Command
	build_type=$2 #Get the Build Command

	local images_success=$MAKE_SUCCESS

	if [ ! -z "$isprint" ]
	then
		return $images_success
	fi

	cd $SDK_ROOT

	binary_list=$(get_build_item_details "" "$build_type" $GET_ITEM_IMAGENAME_MAKEALL)
	for img in $binary_list; do
		if [ ! -e $OUTPUT_TARGET_PATH/$img ] ;
		then
			images_success=$MAKE_FAILURE
			break;
		fi
	done

	return $images_success
}

#Verify Build Using Logs
verify_build_using_logs()
{
	isprint=$1
	build_type=$2 #Get the Build Command
	log_file_name=$3
	local log_success_string=""
	if [ ! -z "$isprint" ]
	then
		return $MAKE_SUCCESS
	fi

	log_success_string=$(get_build_item_details "" "$build_type" $GET_ITEM_LOG_SUCCESS)
	
	search_value=$(tail -n100 $log_file_name| grep "$log_success_string")
	if [ $? -eq $MAKE_SUCCESS ]
	then
		return $MAKE_SUCCESS
	else
		return $MAKE_FAILURE
	fi
}

#Build and Verify
build_and_verify()
{
	isprint=$1 #Get the Print Command
	build_type=$2 #Get the Build Type
	local result=$ERROR_SUCCESS
	local retry_left=$ERROR_MIN_RETRY
	local build_success=$MAKE_FAILURE
	local build_result=$MAKE_SUCCESS
	local log_verify_result=$MAKE_SUCCESS
	local image_verify_result=$MAKE_SUCCESS
	local confirm_to_continue

	while [[ $build_success != $MAKE_SUCCESS ]]; do
		make_images "$isprint" "$build_type"
		build_result=$?
		image_lookup "$isprint" "$build_type"
		image_verify_result=$?
		verify_build_using_logs "$isprint" "$build_type" "$BUILDROOT/$BUILD_LOG_FILE_NAME"
		log_verify_result=$?

		if [[ $build_result = $MAKE_SUCCESS ]] && [[ $image_verify_result  = $MAKE_SUCCESS ]] && [[ $log_verify_result  = $MAKE_SUCCESS ]] ;
		then
			build_success=$MAKE_SUCCESS
		else
			build_success=$MAKE_FAILURE
		fi

		if [ -z "$isprint" ]
		then

			case $build_success in
				[$MAKE_SUCCESS] )
					echo -e "$YELLOW BUILD SUCCESS! $build_type All Images Identified in $OUTPUT_TARGET_PATH path $ENDCOLOR";;
				* )
					echo -e "$RED ERROR! $build_type BUILD FAILED! Retry Build ($retry_left retry left)$ENDCOLOR";
					echo -e "$RED ERROR! Build Result! Make-$build_result, Img-$image_verify_result, Log-$log_verify_result $ENDCOLOR";
					msg="Confirm for Retry the Build"
					confirm_to_continue=$ERROR_SUCCESS
					wait_for_user $msg
					confirm_to_continue=$?
					if [[ "$confirm_to_continue" != "$ERROR_SUCCESS" ]]
					then
						break;
					fi
			esac
		fi

		retry_left=$(expr $retry_left - 1)
		if test $retry_left -lt 0
		then
			break;
		fi
	done

	if [[ $build_success != $MAKE_SUCCESS ]];
	then
		result=$ERROR_FAILURE
	fi

	return $result
}

#Clean the Build
build_clean()
{
	isprint=$1 #Get the Print Command

	setup_env $isprint

	$isprint \
	make clean

	$isprint \
	rm -rf $OUTPUT_DIR
}

#Create BackUp
create_backup()
{
	BACKUP_FILE_NAME="$BUILDNAME""_"$(date +"%y%m%d_%H%M%S").tar.gz

	isprint=$1 #Get the Print Command

	if [ -z "$isprint" ]
	then
		echo "" > $BUILDROOT/$BACKUP_DIR_CONFIG_FILE
		for omit_dir in $BACKUP_DIR_CONFIG; do
			$isprint \
			echo $omit_dir >> $BUILDROOT/$BACKUP_DIR_CONFIG_FILE
		done

		cd $SDK_ROOT

		cat $BUILDROOT/$BACKUP_DIR_CONFIG_FILE

		tar --checkpoint="$CHECK_POINT_VAL" \
			-zcf "$BUILDROOT/$BACKUP_FILE_NAME" \
			--exclude-from="$BUILDROOT/$BACKUP_DIR_CONFIG_FILE" \
			.

	else
		echo -e $GREEN" This step will take the back up for the entire build \n"\
			"Location : $BUILDROOT/$BUILDNAME""_mmddyy_hhmmss\n"\
			$ENDCOLOR
	fi
}

#Download from CLO
download_clo_release()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS
	force_download=$2

	check_for_clo_rel_exists $isprint
	do_download=$?

	$isprint \
	cd $WORKDIR

	if [[ $do_download -eq $ERROR_SUCCESS ]] || [[ $force_download -eq 1 ]]
	then
		$isprint \
		rm -rf $CLO_CLONE_DIR

		$isprint \
		git clone -n --filter=tree:0 $CLO_REL_PATH $CLO_REL_DIR_NAME

		$isprint \
		cd $CLO_CLONE_DIR

		$isprint \
		git sparse-checkout set --no-cone $CLO_REL_TARGET_DIR_NAME/$CLO_REL_VERSION

		$isprint \
		git checkout
	fi

	$isprint \
	ls -al $WORKDIR $CLO_REL_DIR

	return $result
}

#Final Set of Instructions and Messages
post_setup()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ $RETAIN_OLD_BIN -ne 1 ]
	then
		BIN_FILE_NAME="$BUILDNAME"
	else
		BIN_FILE_NAME="$BUILDNAME""_""$(date +"%y%m%d_%H%M%S")"
	fi

	$isprint \
	cd $SDK_ROOT

	$isprint \
	sudo chmod 777 $OUTPUT_TARGET_PATH

	$isprint \
	sudo chmod 777 $SAMPLE_BIN_DIR

	setup_bin_dir $isprint

	$isprint \
	cd $BIN_DIR

	binary_list=$(get_build_item_details "" "" $GET_ITEM_IMAGENAME)
	for img in $binary_list; do
		$isprint \
		cp $OUTPUT_TARGET_PATH/$img $BIN_DIR
	done

	$isprint \
	cp $CLO_REL_DIR/$FLASHING_SCRIPT $BIN_DIR

	$isprint \
	zip -r $BIN_FILE_NAME.zip . -i $binary_list $FLASHING_SCRIPT

	$isprint \
	zipinfo $BIN_DIR/$BIN_FILE_NAME.zip

	$isprint \
	ls -al $BIN_DIR

	return $result
}

#Final Set of Instructions and Messages
exit_step()
{
	cd $WORKDIR
}

#===============================================================================#
#Utility Functions
#===============================================================================#
color()
{
	echo -e $1
}

print_line()
{
	echo -e $RED"==============================================================================================================================================================="$ENDCOLOR
}

print_limits()
{
	intent=$1
	msg=$2
	print_line
	echo -e $YELLOW $@
	print_line
}

wait_for_user()
{
	msg=$@
	color $MAGENTA $UNDER
	# echo $msg
	local confirm_to_continue
	confirm_to_continue=$ERROR_SUCCESS
	if [ "$ENABLE_PROMTS" = "$SET_VALUE" ]
	then
		check_for_confirmation $msg
		confirm_to_continue=$?
		# read -p "Enter to Continue"
	fi
	color $ENDCOLOR

	return $confirm_to_continue
}

exectute_cmd()
{
	local confirm_to_continue
	local result=$ERROR_SUCCESS
	confirm_to_continue=$ERROR_SUCCESS
	call_fn=$1
	display_msg=$2
	print_limits $display_msg Commands
	color $CYAN
	$call_fn "echo" $3
	if [ "$PRINT_ONLY" != "$SET_VALUE" ]
	then
		print_line
		wait_for_user "Please verify the instructions and proceed with execution"
		confirm_to_continue=$?
		if [ $confirm_to_continue -eq $ERROR_SUCCESS ]
		then
			print_limits $display_msg Begin
			$call_fn "" $3
			result=$?
			print_limits $display_msg End
		fi
	fi
	return $result
}

#===============================================================================#
#Color Defines
#===============================================================================#
set_colors()
{
	UNDER='\e[4m'
	RED='\e[31;1m'
	GREEN='\e[32;1m'
	YELLOW='\e[33;1m'
	BLUE='\e[34;1m'
	MAGENTA='\e[35;1m'
	CYAN='\e[36;1m'
	WHITE='\e[37;1m'
	ENDCOLOR='\e[0m'
}

#===============================================================================#
#Build Routine
#===============================================================================#
setup_and_build()
{
	exectute_cmd start_instructions "Instructions" &&
	exectute_cmd install_tools "Setting Up the Tools Required" &&
	exectute_cmd setup_git "Setting Up GIT" &&
	exectute_cmd download_clo_release "Download Scripts and Patches from CLO" "$FORCE_DOWNLOAD" &&
	exectute_cmd setup_root "Setting Up the Root Folder Stucture" &&
	exectute_cmd setup_tool_chain "Setting Up Tool Chain" &&
	exectute_cmd setup_sdk "Setting Up SDK" &&
	exectute_cmd apply_patches "Applying the Patches2" "$PATCH2_DIR_NAME" &&
	exectute_cmd build_all "Building All Images" &&
	exectute_cmd post_setup "Finishing Build Setup"
}

#===============================================================================#
# Update Functions
#===============================================================================#
update_build_name()
{
	local value
	echo "Current Value :" $BUILDNAME
	read -r -p "Enter new Build Name : " value
	BUILDNAME=$value
	echo "New Value :" $BUILDNAME
	_reconfig
}

update_num_make_jobs()
{
	local value
	echo "Current Value :" $MAKE_JOB_THREADS
	read -r -p "Enter new Num Make Jobs : " value
	MAKE_JOB_THREADS=$value
	echo "New Value :" $MAKE_JOB_THREADS
	_reconfig
}

update_enable_disable_promts()
{
	local value
	echo "Current Value :" $ENABLE_PROMTS
	read -r -p "Enable/Disable Promts(0/1) : " value
	ENABLE_PROMTS=$value
	echo "New Value :" $ENABLE_PROMTS
	_reconfig
}

update_enable_print_only()
{
	local value
	echo "Current Value :" $PRINT_ONLY
	read -r -p "Enable Print Only (0/1) : " value
	PRINT_ONLY=$value
	echo "New Value :" $PRINT_ONLY
	_reconfig
}

update_no_tool_chain_update()
{
	local value
	echo "Current Value :" $NO_TOOL_CHAIN_UPDATE
	read -r -p "Enable/Disable Tool Chain Update(0/1) : " value
	NO_TOOL_CHAIN_UPDATE=$value
	echo "New Value :" $NO_TOOL_CHAIN_UPDATE
	_reconfig
}

update_git_config_name()
{
	local value
	echo "Current Value :" $GIT_USER_NAME
	read -r -p "Enter Git User Name : " value
	GIT_USER_NAME=$value
	echo "New Value :" $GIT_USER_NAME
	_reconfig
}

update_git_config_email()
{
	local value
	echo "Current Value :" $GIT_USER_EMAIL
	read -r -p "Enter Git User Email : " value
	GIT_USER_EMAIL=$value
	echo "New Value :" $GIT_USER_EMAIL
	_reconfig
}

update_force_download()
{
	local value
	echo "Current Value :" $FORCE_DOWNLOAD
	read -r -p "Enable/Disable Force Download (0/1) : " value
	FORCE_DOWNLOAD=$value
	echo "New Value :" $FORCE_DOWNLOAD
	_reconfig
}

update_clo_dir_name()
{
	local value
	echo "Current Value :" $CLO_REL_DIR_NAME
	read -r -p "Enter CLO Directory name : " value
	CLO_REL_DIR_NAME=$value
	echo "New Value :" $CLO_REL_DIR_NAME
	_reconfig
}

update_clo_target_dir_name()
{
	local value
	echo "Current Value :" $CLO_REL_TARGET_DIR_NAME
	read -r -p "Enter CLO Target Directory name : " value
	CLO_REL_TARGET_DIR_NAME=$value
	echo "New Value :" $CLO_REL_TARGET_DIR_NAME
	_reconfig
}

update_clo_version_name()
{
	local value
	echo "Current Value :" $CLO_REL_VERSION
	read -r -p "Enter CLO Version name : " value
	CLO_REL_VERSION=$value
	echo "New Value :" $CLO_REL_VERSION
	_reconfig
}

update_clo_release_path()
{
	local value
	echo "Current Value :" $CLO_REL_PATH
	read -r -p "Enter CLO Release Path : " value
	CLO_REL_PATH=$value
	echo "New Value :" $CLO_REL_PATH
	_reconfig
}
update_toolchain_file_name()
{
	local value
	echo "Current Value :" $TOOLCHAIN_FILE
	read -r -p "Enter Tool Chain File name : " value
	TOOLCHAIN_FILE=$value
	echo "New Value :" $TOOLCHAIN_FILE
	_reconfig
}

update_sdk_file_name()
{
	local value
	echo "Current Value :" $SDK_FILE
	read -r -p "Enter SDK File name : " value
	SDK_FILE=$value
	echo "New Value :" $SDK_FILE
	_reconfig
}

update_sdk_directory_name()
{
	local value
	echo "Current Value :" $SDK_DIR
	read -r -p "Enter SDK Directory name : " value
	SDK_DIR=$value
	echo "New Value :" $SDK_DIR
	_reconfig
}


update_local_repo_name()
{
	local value
	echo "Current Value :" $LOCAL_REPO_FILE
	read -r -p "Enter Local Repo name : " value
	LOCAL_REPO_FILE=$value
	echo "New Value :" $LOCAL_REPO_FILE
	_reconfig
}

update_use_local()
{
	local value
	echo "Current Value :" $USE_LOCAL
	read -r -p "Enable/Disable Local Repo(0/1) : " value
	USE_LOCAL=$value
	echo "New Value :" $USE_LOCAL
	_reconfig
}

update_retain_old_bin()
{
	local value
	echo "Current Value :" $RETAIN_OLD_BIN
	read -r -p "Enable/Disable Retain Final Image (0/1) : " value
	RETAIN_OLD_BIN=$value
	echo "New Value :" $RETAIN_OLD_BIN
	_reconfig
}

update_error_min_retry()
{
	local value
	echo "Current Value :" $ERROR_MIN_RETRY
	read -r -p "Enter Number of Minimum Retry  : " value
	ERROR_MIN_RETRY=$value
	echo "New Value :" $ERROR_MIN_RETRY
	_reconfig
}

reset_config()
{
	msg="About to reset config"
	local confirm_to_continue
	confirm_to_continue=$ERROR_SUCCESS
	check_for_confirmation $msg
	confirm_to_continue=$?
	if [ $confirm_to_continue -eq $ERROR_SUCCESS ]
	then
		_config
		_reconfig
	fi
}

#===============================================================================#
# Menu Config
#===============================================================================#
show_main_menu()
{
	display_msg="$BUILDNAME MAIN MENU"
	print_limits $display_msg
	echo "  1. Setup New Build"
	echo "  2. Print All Configs"
	echo "  3. Make All"
	echo "  4. Clean All"
	echo "  5. Prepare Final Images"
	echo "  6. Create a Backup"
	echo "  7. Download from CLO"
	echo "  8. Make ABoot Image"
	echo "  9. Make Kernel Image"
	echo " 10. Make System Image"
	echo " 11. Make Sample Apps"
	echo " 99. Show Config Menu"
	echo "100. Quit"
}

#===============================================================================#
# Config Menu
#===============================================================================#
show_config_menu()
{
	display_msg="$BUILDNAME CONFIGURATION MENU"
	print_limits $display_msg
	echo "  0. Main Menu"
	echo "  1. Print All Configs"
	echo "  2. Reset All Configuration"
	echo "  3. Update Build Name"
	echo "  4. Enable/Disable Promts"
	echo "  5. Enable Print Only"
	echo "  6. Update Force Download Option"
	echo "  7. Update Git Config Email"
	echo "  8. Update Git Config Name"
	echo "  9. Update Num Make Jobs"
	echo " 10. Update USE LOCAL"
	echo " 11. Update Local Repo Name"
	echo " 12. Retain Previous Images"
	echo " 13. Update SDK File Name"
	echo " 14. Update SDK Directory Name"
	echo " 15. Update CLO Directory Name"
	echo " 16. Update CLO Target Directory Name"
	echo " 17. Update CLO Release Path"
	echo " 18. Update CLO Version Name"
	echo " 19. Update Num Minimum Retry "
	echo " 20. Enable/Disable Tool Chain update"
	echo " 21. Update Tool Chain Name"
	echo "100. Quit"
}

execute_functions()
{
	local curr_menu_id
	selection=$1
	curr_menu_id=$2
	case $curr_menu_id in
		$MENU_MAIN)
		case $selection in
			1 ) setup_and_build;;
			2 ) print_config;;
			3 ) exectute_cmd build_all "Make All Images"  ;;
			4 ) exectute_cmd build_clean "Clean the Entire Build";;
			5 ) exectute_cmd post_setup "Prepare Final Images";;
			6 ) exectute_cmd create_backup "Create a Backup";;
			7 ) exectute_cmd download_clo_release "Download from CLO" 1;;
			8 ) exectute_cmd build_and_verify "Building ABoot Image" $ABOOT_BUILD_TYPE ;;
			9 ) exectute_cmd build_and_verify "Building Kernel Image" $BOOT_BUILD_TYPE ;;
			10) exectute_cmd build_and_verify "Building System Image" $SYSTEM_BUILD_TYPE ;;
			11) exectute_cmd build_and_verify "Building Sample Apps" $SAMPLE_APPS_BUILD_TYPE ;;
			99) curr_menu_id=$MENU_CONFIG;;
			* ) echo "Incorrect Option Selected" && sleep 1;;
			esac
		;;
		$MENU_CONFIG) case $selection in
			 0) curr_menu_id=$MENU_MAIN;;
			 1) print_config;;
			 2) reset_config;;
			 3) update_build_name;;
			 4) update_enable_disable_promts;;
			 5) update_enable_print_only;;
			 6) update_force_download;;
			 7) update_git_config_email;;
			 8) update_git_config_name;;
			 9) update_num_make_jobs;;
			10) update_use_local;;
			11) update_local_repo_name;;
			12) update_retain_old_bin;;
			13) update_sdk_file_name;;
			14) update_sdk_directory_name;;
			15) update_clo_dir_name;;
			16) update_clo_target_dir_name;;
			17) update_clo_release_path;;
			18) update_clo_version_name;;
			19) update_error_min_retry;;
			20) update_no_tool_chain_update;;
			21) update_toolchain_file_name;;
			* ) echo "Incorrect Option Selected" && sleep 1;;
			esac
		;;
	esac
	return $curr_menu_id;
}

read_selection()
{
	local selection
	end_val=$1
	exit_val=$2
	read -r -p "Enter Selection : " selection
	if ([ -z "$selection" ])
	then
		selection=-1
	fi
	if((($selection<0 || $selection>$end_val) && ($selection!=$exit_val)))
	then
		selection=-1
	fi
	return $selection;
}

#===============================================================================#
# Main Function
#===============================================================================#
_main()
{
	_config
	_reconfig
	set_colors
	num_selections=100
	exit_val=100
	curr_menu_id=$MENU_MAIN
	user_input=$1

	if [[ ! -z $user_input ]]
	then
		ENABLE_PROMTS=$CLEAR_VALUE
		execute_functions $user_input $MENU_MAIN
		_config
		_reconfig
		return
	fi

	while true
	do
		case $curr_menu_id in
			$MENU_MAIN) show_main_menu;;
			$MENU_CONFIG) show_config_menu;;
		esac
		read_selection $num_selections $exit_val
		selection=$?
		if [ $selection -eq $exit_val ]
		then
			echo "Exiting!!" && sleep 2
			break;
		fi
		execute_functions $selection $curr_menu_id
		curr_menu_id=$?
	done
	exit_step
}
_main $1
