#/bin/bash

#===============================================================================#
#Variables and Config Parameters
#===============================================================================#
_config()
{
	WORKDIR=`pwd`
	BUILDNAME="SC66"
	REPO_JOB_THREADS=32
	MAKE_JOB_THREADS=8
	ENABLE_PROMTS=1
	PRINT_ONLY=0
	FORCE_DOWNLOAD=1
	USE_LOCAL=0
	RETAIN_OLD_BIN=0
	CHECK_POINT_VAL=8192
	NO_TOOL_CHAIN_UPDATE=0
	ERROR_SUCCESS=0
	ERROR_FAILURE=1
	ERROR_MIN_RETRY=5
	MAKE_SUCCESS=0
	MAKE_FAILURE=2
	SET_VALUE=1
	CLEAR_VALUE=0
	MENU_MAIN=0
	MENU_CONFIG=1
	GET_ITEM_DEFAULT=0
	GET_ITEM_BUILDCMD=1
	GET_ITEM_IMAGENAME=2
	GET_ITEM_IMAGENAME_MAKEALL=3
	GET_ITEM_LOG_SUCCESS=4
	GIT_USER_NAME="dev"
	GIT_USER_EMAIL="dev@$BUILDNAME"
	GIT_LOG_COUNT=15
	SDK_FILE="SC66_Android10.0_Quectel_SDK_r008_20200604.tar.gz"
	SDK_DIR="Android_SDK_SC66"
	PATCHES3_FILE="sc66-qdn_relv1.0.zip"
	LOCAL_REPO_FILE="Android_SDK_SC66.tar.gz"
	OUTPUT_DIR_NAME="out"
	OUTPUT_TARGET_DIR="target/product/sdm660_64/"
	BIN_DIR_NAME="bin"
	FLASHING_SCRIPT="load.bat"
	CLO_REL_DIR_NAME="sc66"
	CLO_REL_TARGET_DIR_NAME="sc66"
	CLO_REL_VERSION="SC66-Relv2.0"
	CLO_REL_PATH="https://git.codelinaro.org/clo/startup-kits/patches.git"
	CLO_SI_REL_TAG="LA.UM.8.2.1.r1-04200-sdm660.0.xml"
	CLO_URL="https://git.codelinaro.org"
	CLO_MANIFEST="clo/la/platform/manifest.git"
	CLO_REPO="clo/tools/repo.git"
	PATCH_DIR_NAME="patches"
	PATCH1_DIR_NAME_SUFFIX=""
	PATCH2_DIR_NAME_SUFFIX="2"
	PATCH3_DIR_NAME_SUFFIX="3"
	PATCHOS_DIR_NAME_SUFFIX="os"
	_config_package_list
	_config_backup_dir
	_config_binary_list
}

_reconfig()
{
	BUILDROOT="$WORKDIR/$BUILDNAME"
	SDK_ROOT="$BUILDROOT/$SDK_DIR"
	OUTPUT_DIR="$SDK_ROOT/$OUTPUT_DIR_NAME"
	OUTPUT_TARGET_PATH="$OUTPUT_DIR/$OUTPUT_TARGET_DIR"
	BIN_DIR="$SDK_ROOT/$BIN_DIR_NAME"
	CLO_CLONE_DIR="$WORKDIR/$CLO_REL_DIR_NAME"
	CLO_REL_DIR="$CLO_CLONE_DIR/$CLO_REL_TARGET_DIR_NAME/$CLO_REL_VERSION"
	PATCH1_DIR_NAME="$PATCH_DIR_NAME$PATCH1_DIR_NAME_SUFFIX"
	PATCH2_DIR_NAME="$PATCH_DIR_NAME$PATCH2_DIR_NAME_SUFFIX"
	PATCH3_DIR_NAME="$PATCH_DIR_NAME$PATCH3_DIR_NAME_SUFFIX"
	PATCHOS_DIR_NAME="$PATCH_DIR_NAME$PATCHOS_DIR_NAME_SUFFIX"
	PATCH_DIR1="$CLO_REL_DIR/$PATCH1_DIR_NAME"
	PATCH_DIR2="$CLO_REL_DIR/$PATCH2_DIR_NAME"
	PATCH_DIR3="$CLO_REL_DIR/$PATCH3_DIR_NAME"
	PATCH_DIR_OS="$CLO_REL_DIR/$PATCHOS_DIR_NAME"
	CLO_MANIFEST_URL="$CLO_URL/$CLO_MANIFEST"
	CLO_REPO_URL="$CLO_URL/$CLO_REPO"
}

_config_package_list()
{
	PACKAGE_LIST="          \
		bc                  \
		bison               \
		build-essential     \
		curl                \
		flex                \
		g++-multilib        \
		git                 \
		git-core            \
		gnupg               \
		gperf               \
		htop                \
		lftp                \
		lib32ncurses5-dev   \
		lib32readline6-dev  \
		lib32z1-dev         \
		libc6-dev           \
		libgl1-mesa-dev     \
		libssl-dev          \
		libx11-dev          \
		libxml2-utils       \
		mingw-w64           \
		openjdk-8*          \
		phablet-tools       \
		python-markdown     \
		tree                \
		tofrodos            \
		x11proto-core-dev   \
		xsltproc            \
		zip                 \
		zlib1g-dev          \
		"
}

_config_backup_dir()
{
	BACKUP_DIR_CONFIG_FILE="BackupCfg.txt"
	BACKUP_DIR_CONFIG="    \
		./out              \
		./.repo            \
		.git/*             \
		"
}

_config_binary_list()
{
	DEFAULT_LOG_SUCCESS_STRING="#### build completed successfully"
	DISABLE_LOG_VERIFY=""
	ABOOT_BUILD_TYPE="1_aboot"
	ABOOT_BUILD_CMD="aboot"
	ABOOT_IMAGE_NAME="abl.elf"
	ABOOT_MAKE_ALL="1"
	ABOOT_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	BOOT_BUILD_TYPE="2_boot"
	BOOT_BUILD_CMD="kernel"
	BOOT_IMAGE_NAME="boot.img"
	BOOT_MAKE_ALL="1"
	BOOT_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	SYSTEM_BUILD_TYPE="3_system"
	SYSTEM_BUILD_CMD="systemimage"
	SYSTEM_IMAGE_NAME="system.img"
	SYSTEM_MAKE_ALL="1"
	SYSTEM_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	USERDATA_BUILD_TYPE="4_userdata"
	USERDATA_BUILD_CMD="userdataimage"
	USERDATA_IMAGE_NAME="userdata.img"
	USERDATA_MAKE_ALL="1"
	USERDATA_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	VENDOR_BUILD_TYPE="5_vendor"
	VENDOR_BUILD_CMD="vendorimage"
	VENDOR_IMAGE_NAME="vendor.img"
	VENDOR_MAKE_ALL="1"
	VENDOR_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	SUPER_BUILD_TYPE="6_super"
	SUPER_BUILD_CMD=""
	SUPER_IMAGE_NAME="super.img"
	SUPER_MAKE_ALL="0"
	SUPER_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	DTBO_BUILD_TYPE="7_dtbo"
	DTBO_BUILD_CMD=""
	DTBO_IMAGE_NAME="dtbo.img"
	DTBO_MAKE_ALL="1"
	DTBO_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	VB_META_BUILD_TYPE="8_vbmeta"
	VB_META_BUILD_CMD=""
	VB_META_IMAGE_NAME="vbmeta.img"
	VB_META_MAKE_ALL="0"
	VB_META_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
	VB_META_SYSTEM_BUILD_TYPE="9_vbmeta_system"
	VB_META_SYSTEM_BUILD_CMD=""
	VB_META_SYSTEM_IMAGE_NAME="vbmeta_system.img"
	VB_META_SYSTEM_MAKE_ALL="0"
	VB_META_SYSTEM_MAKE_SUCCESS_STRING="$DEFAULT_LOG_SUCCESS_STRING"
}

#===============================================================================#
# Display all Config Parameters
#===============================================================================#
print_config()
{
	echo "WORKDIR                 = "$WORKDIR
	echo "BUILDNAME               = "$BUILDNAME
	echo "MAKE_JOB_THREADS        = "$MAKE_JOB_THREADS
	echo "REPO_JOB_THREADS        = "$REPO_JOB_THREADS
	echo "ENABLE_PROMTS           = "$ENABLE_PROMTS
	echo "PRINT_ONLY              = "$PRINT_ONLY
	echo "FORCE_DOWNLOAD          = "$FORCE_DOWNLOAD
	echo "USE_LOCAL               = "$USE_LOCAL
	echo "RETAIN_OLD_BIN          = "$RETAIN_OLD_BIN
	echo "NO_TOOL_CHAIN_UPDATE    = "$NO_TOOL_CHAIN_UPDATE
	echo "GIT_USER_NAME           = "$GIT_USER_NAME
	echo "GIT_USER_EMAIL          = "$GIT_USER_EMAIL
	echo "BUILDROOT               = "$BUILDROOT
	echo "CLO_URL                 = "$CLO_URL
	echo "CLO_MANIFEST            = "$CLO_MANIFEST
	echo "CLO_MANIFEST_URL        = "$CLO_MANIFEST_URL
	echo "GIT_LOG_COUNT           = "$GIT_LOG_COUNT
	echo "CLO_REPO                = "$CLO_REPO
	echo "CLO_REPO_URL            = "$CLO_REPO_URL
	echo "CLO_SI_REL_TAG          = "$CLO_SI_REL_TAG
	echo "SDK_DIR                 = "$SDK_DIR
	echo "SDK_ROOT                = "$SDK_ROOT
	echo "SDK_FILE                = "$SDK_FILE
	echo "PATCHES3_FILE           = "$PATCHES3_FILE
	echo "LOCAL_REPO_FILE         = "$LOCAL_REPO_FILE
	echo "OUTPUT_DIR_NAME         = "$OUTPUT_DIR_NAME
	echo "OUTPUT_DIR              = "$OUTPUT_DIR
	echo "OUTPUT_TARGET_DIR       = "$OUTPUT_TARGET_DIR
	echo "OUTPUT_TARGET_PATH      = "$OUTPUT_TARGET_PATH
	echo "BIN_DIR_NAME            = "$BIN_DIR_NAME
	echo "BIN_DIR                 = "$BIN_DIR
	echo "FLASHING_SCRIPT         = "$FLASHING_SCRIPT
	echo "PATCH_DIR_NAME          = "$PATCH_DIR_NAME
	echo "PATCH_DIR1              = "$PATCH_DIR1
	echo "PATCH_DIR2              = "$PATCH_DIR2
	echo "PATCH_DIR3              = "$PATCH_DIR3
	echo "PATCH_DIR_OS            = "$PATCH_DIR_OS
	echo "CLO_REL_DIR_NAME        = "$CLO_REL_DIR_NAME
	echo "CLO_REL_TARGET_DIR_NAME = "$CLO_REL_TARGET_DIR_NAME
	echo "CLO_REL_DIR             = "$CLO_REL_DIR
	echo "CLO_CLONE_DIR           = "$CLO_CLONE_DIR
	echo "CLO_REL_VERSION         = "$CLO_REL_VERSION
	echo "CLO_REL_PATH            = "$CLO_REL_PATH
	echo "PACKAGE_LIST            = "$PACKAGE_LIST
}

check_for_confirmation()
{
	msg=$@
	local confirm_to_continue
	confirm_to_continue=$ERROR_SUCCESS
	if [ "$ENABLE_PROMTS" = "$SET_VALUE" ]
	then
		while true; do
			echo $msg
			read -r -p "Enter Yes(Yy)/No(Nn)/Skip(Ss) :  " answer
			case $answer in
				[Yy]* ) break;;
				[Nn]* ) continue;;
				[Ss]* )
					confirm_to_continue=$ERROR_FAILURE
					break;;
				* ) echo "Please Answer Y to continue";;
			esac
		done
	fi

	return $confirm_to_continue;
}

check_file_copy_confirmation()
{
	msg="${@:3}"
	file_path=$1
	file_path_alt=$2
	if [ "$ENABLE_PROMTS" = "$SET_VALUE" ]
	then
		while true; do
			echo $msg
			read -r -p "Enter (Yy/Nn): " answer
			case $answer in
				[Yy]* )
				if [ ! -e $file_path ] ;
				then
					if [ -e $file_path_alt ]
					then
						cp $file_path_alt $file_path
						break;
					else
						echo -e "$RED ERROR! $file_path or $file_path_alt Not Found $ENDCOLOR";
						continue;
					fi
				else
					break;
				fi
				;;
				[Nn]* ) continue;;
				* ) echo "Please Answer Y to continue";;
			esac
		done
	else
		echo $msg
		if [ ! -e $file_path ]
		then
			if [ -e $file_path_alt ]
			then
				cp $file_path_alt $file_path
			else
				echo -e "$RED ERROR! $file_path or $file_path_alt Not Found $ENDCOLOR";
				exit;
			fi
		fi
	fi
}

check_for_sdk_exists()
{
	isprint=$1
	if [[ "$ENABLE_PROMTS" = "$SET_VALUE" ]] && [[ -z "$isprint" ]]
	then
		while true; do
			if [ ! -e $BUILDROOT ] ;
			then
				break;
			else
				echo "Build Already Exist, Continue with Rename(Y)/Replace(R) or Exit(N)"
				read -r -p "Enter (Yy/Nn/Rr): " answer
				case $answer in
					[Yy]* )
					update_build_name;
					continue;;
					[Rr]* )
					rm -rf $BUILDROOT;
					break;;
					[Nn]* )
					exit;;
				esac
			fi
		done
	else
		$isprint \
		rm -rf $BUILDROOT
	fi
}

check_for_clo_rel_exists()
{
	isprint=$1
	local do_download
	do_download=$ERROR_SUCCESS

	if [[ "$ENABLE_PROMTS" = "$SET_VALUE" ]] && [[ -z "$isprint" ]]
	then
		while true; do
			if [ ! -e $CLO_CLONE_DIR ] ;
			then
				break;
			else
				echo "CLO Release Already Exist, Continue with Rename(Y)/Replace(R)/UseExisting(N)"
				read -r -p "Enter (Yy/Nn/Rr): " answer
				case $answer in
					[Yy]* )
					update_clo_dir_name;
					continue;;
					[Rr]* )
					rm -rf $CLO_CLONE_DIR;
					break;;
					[Nn]* )
					do_download=$ERROR_FAILURE
					break;;
				esac
			fi
		done
	fi

	return $do_download;
}

# Instructions to Begin with
start_instructions()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ -z "$isprint" ]
	then
		msg="Read Confirmation Required"
		# check_for_confirmation $msg
	else
		echo -e $GREEN"This Script is used for demostrating the instructions\n"\
			"All Instructions used in the scripts are displayed in screen for future reference\n"\
			"Therefore the script is not mandatory\n"\
			$ENDCOLOR
	fi

	return $result
}

# Install Tools- Python and Virtual Env
install_tools()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	sudo add-apt-repository -y -u ppa:git-core/ppa

	$isprint \
	sudo apt update

	$isprint \
	sudo apt --assume-yes install $PACKAGE_LIST

	return $result
}

#Setup Root
setup_root()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	mkdir -p $PATCH_DIR1 $PATCH_DIR3

	check_for_sdk_exists $isprint

	$isprint \
	mkdir -p $BUILDROOT

	$isprint \
	cd $BUILDROOT

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required $SDK_FILE to $PATCH_DIR1/$SDK_FILE or $WORKDIR/$SDK_FILE"
		check_file_copy_confirmation $PATCH_DIR1/$SDK_FILE $WORKDIR/$SDK_FILE $msg
	else
		echo -e $RED"MANDATORY! Manual Step \n"$GREEN\
			"Copy the SDK File ("$SDK_FILE") to \n"$PATCH_DIR1/$SDK_FILE "\n or\n"$WORKDIR/$SDK_FILE \
			"before proceeding to next steps"$ENDCOLOR
	fi

	if [ -z "$isprint" ]
	then
		msg="Copy Confirmation Required $PATCHES3_FILE to $PATCH_DIR3/$PATCHES3_FILE or $WORKDIR/$PATCHES3_FILE"
		check_file_copy_confirmation $PATCH_DIR3/$PATCHES3_FILE $WORKDIR/$PATCHES3_FILE $msg
	else
		echo -e $RED"MANDATORY! Manual Step \n"$GREEN\
			"Copy the QDN Patch File ("$PATCHES3_FILE") to \n"$PATCH_DIR3/$PATCHES3_FILE"\n or\n"$WORKDIR/$PATCHES3_FILE \
			"before proceeding to next steps"$ENDCOLOR
	fi

	$isprint \
	ls -al $WORKDIR $PATCH_DIR1 $BUILDROOT

	return $result
}

#Setup Tool Chain
setup_tool_chain()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ "$NO_TOOL_CHAIN_UPDATE" = "$SET_VALUE" ]
	then
		return
	fi

	$isprint \
	cd $BUILDROOT

	return $result
}

run_command()
{
	isprint=$1 #Get the Print Command
	local command_name=$2
	local command_to_run=$3
	local confirm_to_continue
	local run_success=$ERROR_FAILURE
	local retry_left=$ERROR_MIN_RETRY

	while [[ $run_success != $ERROR_SUCCESS ]]; do
		$isprint \
		$command_to_run
		run_success=$?

		if [ -z "$isprint" ]
		then

			case $run_success in
				[$ERROR_SUCCESS] )
					echo -e "$YELLOW $command_name Command SUCCESS!\n$command_to_run  $ENDCOLOR";;
				* )
					echo -e "$RED ERROR! $command_name Command FAILED! Retry ($retry_left retry left)\n $command_to_run  $ENDCOLOR";
					msg="Confirm for Retry"
					confirm_to_continue=$ERROR_SUCCESS
					wait_for_user $msg
					confirm_to_continue=$?
					if [[ "$confirm_to_continue" != "$ERROR_SUCCESS" ]]
					then
						break;
					fi
			esac
		fi

		retry_left=$(expr $retry_left - 1)
		if test $retry_left -lt 0
		then
			break;
		fi

	done

	return $run_success;
}

#Setup SDK
setup_sdk_repo()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS
	local repo_command_type="REPO"
	local repo_init_cmd="repo init -u $CLO_MANIFEST_URL -b release -m ${CLO_SI_REL_TAG} --repo-url=$CLO_REPO_URL --repo-branch=qc-stable --depth=1"
	local repo_sync_cmd="repo sync -cj$REPO_JOB_THREADS -q --no-tags --no-clone-bundle"

	$isprint \
	mkdir -p $SDK_ROOT

	$isprint \
	cd $SDK_ROOT

	if [ $result = $ERROR_SUCCESS ]; then
		run_command "$isprint" "$repo_command_type" "$repo_init_cmd"
		result=$?
	fi

	if [ $result = $ERROR_SUCCESS ]; then
		run_command "$isprint" "$repo_command_type" "$repo_sync_cmd"
		result=$?
	fi

	$isprint \
	ls -al $BUILDROOT $SDK_ROOT

	return $result
}

#Setup SDK
setup_sdk_local()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	mkdir -p $SDK_ROOT

	$isprint \
	cd $SDK_ROOT

	$isprint \
	tar --checkpoint=$CHECK_POINT_VAL -xvf $WORKDIR/$LOCAL_REPO_FILE -C $SDK_ROOT

	$isprint \
	ls -al $BUILDROOT $SDK_ROOT

	return $result
}

#Setup SDK
setup_sdk()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ $USE_LOCAL -ne 1 ]
	then
		setup_sdk_repo $isprint
		result=$?
	else
		setup_sdk_local $isprint
		result=$?
	fi

	return $result
}

#Apply Patches1
setup_sdk_part2()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	cd $BUILDROOT

	$isprint \
	tar --checkpoint=$CHECK_POINT_VAL -xvf $PATCH_DIR1/$SDK_FILE -C $SDK_ROOT

	$isprint \
	ls -al $BUILDROOT $SDK_ROOT

	return $result
}

#Extract Patches3
extract_patches3()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	cd $PATCH_DIR3

	$isprint \
	find . -type f -not -name $PATCHES3_FILE -delete

	$isprint \
	find . -type d -empty -delete

	$isprint \
	unzip -qo $PATCHES3_FILE -d $PATCH_DIR3 "*/$PATCH3_DIR_NAME/*"

	$isprint \
	mv $PATCH_DIR3/*/$PATCH3_DIR_NAME/* $PATCH_DIR3

	$isprint \
	find . -type d -empty -delete

	$isprint \
	ls -al $PATCH_DIR3

	$isprint \
	tree $CLO_REL_DIR

	return $result
}

display_git_status()
{
	isprint=$1 #Get the Print Command

	$isprint \
	git log --oneline -"$GIT_LOG_COUNT"

	$isprint \
	git status
}

#Setup git
setup_git()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	$isprint \
	cd $WORKDIR

	$isprint \
	git config --global init.defaultBranch main

	$isprint \
	git config --global user.name $GIT_USER_NAME

	$isprint \
	git config --global user.email $GIT_USER_EMAIL

	$isprint \
	ls -al $WORKDIR

	return $result
}

#Initialize git for each project
initialize_git_folder()
{
	isprint=$1 #Get the Print Command
	PATCH_DIR_NAME=$2
	PATCH_SUB_DIR=$3
	PATCH_SOURCE_DIR=$CLO_REL_DIR/$PATCH_DIR_NAME
	PATCH_SOURCE_SUB_DIR=$PATCH_SOURCE_DIR/$PATCH_SUB_DIR
	PATCH_TARGET_DIR=$SDK_ROOT/$PATCH_SUB_DIR

	if [ -z "$isprint" ]
	then
		echo -e "$YELLOW  Performing InitialCommit on $PATCH_TARGET_DIR ... $ENDCOLOR"
	fi

	if [ ! -e $PATCH_TARGET_DIR ] ;
	then
		if [ -z "$isprint" ]
		then
			echo -e "$RED $PATCH_TARGET_DIR does not exist in SDK_ROOT:$SDK_ROOT $ENDCOLOR"
		fi

		$isprint \
		mkdir -p $PATCH_TARGET_DIR

	fi

	$isprint \
	cd $PATCH_TARGET_DIR

	$isprint \
	git init

	$isprint \
	git add .

	$isprint \
	git commit -m '"'"[$BUILDNAME Initial Commit] Project:$PATCH_SUB_DIR"'"' -s
}

#Applying the Patches
apply_patches()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS
	PATCH_DIR_NAME=$2
	PATCH_DIR=$CLO_REL_DIR/$PATCH_DIR_NAME

	cd $PATCH_DIR

	PATCH_TARGET_DIR_NAME=$(echo "$PATCH_DIR_NAME" | sed -r 's/\b[^/]+[/]//')

	android_patch_list=$( find $PATCH_DIR/* -type f -name '*.patch' | sed 's/^.*\/'$PATCH_TARGET_DIR_NAME'\(.*\)\/[^/]\+$/\1\/.\//' |sort -u)

	cd $SDK_ROOT

	for patch_dir in $android_patch_list; do
		patch_list=$(find $PATCH_DIR/$patch_dir -type f -name "*.patch" | sort) &&

		initialize_git_folder "$isprint" "$PATCH_DIR_NAME" "$patch_dir"
		for patch in $patch_list; do
			$isprint \
			git am $patch
		done
		display_git_status $isprint
	done

	return $result
}

setup_bin_dir()
{
	isprint=$1 #Get the Print Command

	if [ ! -e $BIN_DIR ] ;
	then
		$isprint \
		mkdir -p $BIN_DIR
	fi
}

#Setup Enviorment
setup_env()
{
	isprint=$1 #Get the Print Command

	$isprint \
	cd $SDK_ROOT

	$isprint \
	source build/envsetup.sh

	$isprint \
	lunch sdm660_64-userdebug

	setup_bin_dir $isprint
}

get_build_item_details()
{
	isprint=$1
	build_type=$2
	return_item=$3
	local image_name=""
	local image_name_make_all=""
	local build_cmd=""
	local build_type_r=""
	local log_success_string="$DEFAULT_LOG_SUCCESS_STRING"

	declare -A build_cmd_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_BUILD_CMD
		[$BOOT_BUILD_TYPE]=$BOOT_BUILD_CMD
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_BUILD_CMD
		[$USERDATA_BUILD_TYPE]=$USERDATA_BUILD_CMD
		[$VENDOR_BUILD_TYPE]=$VENDOR_BUILD_CMD
		[$SUPER_BUILD_TYPE]=$SUPER_BUILD_CMD
		[$DTBO_BUILD_TYPE]=$DTBO_BUILD_CMD
		[$VB_META_BUILD_TYPE]=$VB_META_BUILD_CMD
		[$VB_META_SYSTEM_BUILD_TYPE]=$VB_META_SYSTEM_BUILD_CMD
	)

	declare -A image_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_IMAGE_NAME
		[$BOOT_BUILD_TYPE]=$BOOT_IMAGE_NAME
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_IMAGE_NAME
		[$USERDATA_BUILD_TYPE]=$USERDATA_IMAGE_NAME
		[$VENDOR_BUILD_TYPE]=$VENDOR_IMAGE_NAME
		[$SUPER_BUILD_TYPE]=$SUPER_IMAGE_NAME
		[$DTBO_BUILD_TYPE]=$DTBO_IMAGE_NAME
		[$VB_META_BUILD_TYPE]=$VB_META_IMAGE_NAME
		[$VB_META_SYSTEM_BUILD_TYPE]=$VB_META_SYSTEM_IMAGE_NAME
	)

	declare -A make_all_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_MAKE_ALL
		[$BOOT_BUILD_TYPE]=$BOOT_MAKE_ALL
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_MAKE_ALL
		[$USERDATA_BUILD_TYPE]=$USERDATA_MAKE_ALL
		[$VENDOR_BUILD_TYPE]=$VENDOR_MAKE_ALL
		[$SUPER_BUILD_TYPE]=$SUPER_MAKE_ALL
		[$DTBO_BUILD_TYPE]=$DTBO_MAKE_ALL
		[$VB_META_BUILD_TYPE]=$VB_META_MAKE_ALL
		[$VB_META_SYSTEM_BUILD_TYPE]=$VB_META_SYSTEM_MAKE_ALL
	)

	declare -A log_str_list=(
		[$ABOOT_BUILD_TYPE]=$ABOOT_MAKE_SUCCESS_STRING
		[$BOOT_BUILD_TYPE]=$BOOT_MAKE_SUCCESS_STRING
		[$SYSTEM_BUILD_TYPE]=$SYSTEM_MAKE_SUCCESS_STRING
		[$USERDATA_BUILD_TYPE]=$USERDATA_MAKE_SUCCESS_STRING
		[$VENDOR_BUILD_TYPE]=$VENDOR_MAKE_SUCCESS_STRING
		[$SUPER_BUILD_TYPE]=$SUPER_MAKE_SUCCESS_STRING
		[$DTBO_BUILD_TYPE]=$DTBO_MAKE_SUCCESS_STRING
		[$VB_META_BUILD_TYPE]=$VB_META_MAKE_SUCCESS_STRING
		[$VB_META_SYSTEM_BUILD_TYPE]=$VB_META_SYSTEM_MAKE_SUCCESS_STRING
	)

	build_type_list=$(echo "${!build_cmd_list[@]}" | tr ' ' $'\n' | sort -u)

	for type in $build_type_list; do
		if [ "$type" = "$build_type" ]; then
			build_cmd="${build_cmd_list[$type]}"
			image_name="${image_list[$type]}"
			log_success_string="${log_str_list[$type]}"
			build_type_r="$type"
			break;
		fi
		build_cmd+=" ${build_cmd_list[$type]}"
		image_name+=" ${image_list[$type]}"
		build_type_r+=" $type"
		if [ "$SET_VALUE" = "${make_all_list[$type]}" ]; then
			image_name_make_all+=" ${image_list[$type]}"
		fi
	done

	case $return_item in
		$GET_ITEM_BUILDCMD)
			echo $build_cmd;;
		$GET_ITEM_IMAGENAME)
			echo $image_name;;
		$GET_ITEM_IMAGENAME_MAKEALL)
			echo $image_name_make_all;;
		$GET_ITEM_LOG_SUCCESS)
			echo $log_success_string;;
		*)
			echo $build_type_r;;
	esac
}

make_images()
{
	isprint=$1 #Get the Print Command
	build_type=$2 #Get the Build Command
	local build_result=$MAKE_SUCCESS
	local build_command=""

	if [ ! -z "$build_type" ]
	then
		build_command=$(get_build_item_details "" "$build_type" $GET_ITEM_BUILDCMD)
	fi

	setup_env $isprint

	cd $SDK_ROOT

	BUILD_LOG_FILE_NAME="$BUILDNAME""$build_type""_"$(date +"%y%m%d_%H%M%S").log
	if [ -z "$build_command" ]
	then
		make_command="./build.sh dist"
	else
		make_command="make $build_command"
	fi
	make_command+=" -j$MAKE_JOB_THREADS 2>&1"
	log_command="tee $BUILDROOT/$BUILD_LOG_FILE_NAME"
	make_command+=" | $log_command"
	make_command_no_result="$make_command"
	make_command+='; build_result=${PIPESTATUS[0]}'
	if [ -z "$isprint" ]
	then
		eval $make_command
	else
		echo $make_command_no_result
	fi

	if [ ! -z "$build_type" ] && [[ $build_result = $MAKE_SUCCESS ]]
	then
		#Copy the Images Here
		image_name=$(get_build_item_details "" "$build_type" $GET_ITEM_IMAGENAME)
		$isprint \
		cp -rf $OUTPUT_TARGET_PATH/$image_name $BIN_DIR
	fi

	$isprint \
	ls -al $BIN_DIR

	return $build_result
}

#Lookup all images
image_lookup()
{
	isprint=$1 #Get the Print Command
	build_type=$2 #Get the Build Command

	local images_success=$MAKE_SUCCESS

	if [ ! -z "$isprint" ]
	then
		return $images_success
	fi

	cd $SDK_ROOT

	binary_list=$(get_build_item_details "" "$build_type" $GET_ITEM_IMAGENAME_MAKEALL)
	for img in $binary_list; do
		if [ ! -e $OUTPUT_TARGET_PATH/$img ] ;
		then
			images_success=$MAKE_FAILURE
			break;
		fi
	done

	return $images_success
}

#Verify Build Using Logs
verify_build_using_logs()
{
	isprint=$1
	build_type=$2 #Get the Build Command
	log_file_name=$3
	local log_success_string=""
	if [ ! -z "$isprint" ]
	then
		return $MAKE_SUCCESS
	fi

	log_success_string=$(get_build_item_details "" "$build_type" $GET_ITEM_LOG_SUCCESS)
	
	search_value=$(tail -n100 $log_file_name| grep "$log_success_string")
	if [ $? -eq $MAKE_SUCCESS ]
	then
		return $MAKE_SUCCESS
	else
		return $MAKE_FAILURE
	fi
}

#Build and Verify
build_and_verify()
{
	isprint=$1 #Get the Print Command
	build_type=$2 #Get the Build Type
	local result=$ERROR_SUCCESS
	local retry_left=$ERROR_MIN_RETRY
	local build_success=$MAKE_FAILURE
	local build_result=$MAKE_SUCCESS
	local log_verify_result=$MAKE_SUCCESS
	local image_verify_result=$MAKE_SUCCESS
	local confirm_to_continue

	while [[ $build_success != $MAKE_SUCCESS ]]; do
		make_images "$isprint" "$build_type"
		build_result=$?
		image_lookup "$isprint" "$build_type"
		image_verify_result=$?
		verify_build_using_logs "$isprint" "$build_type" "$BUILDROOT/$BUILD_LOG_FILE_NAME"
		log_verify_result=$?

		if [[ $build_result = $MAKE_SUCCESS ]] && [[ $image_verify_result  = $MAKE_SUCCESS ]] && [[ $log_verify_result  = $MAKE_SUCCESS ]] ;
		then
			build_success=$MAKE_SUCCESS
		else
			build_success=$MAKE_FAILURE
		fi

		if [ -z "$isprint" ]
		then

			case $build_success in
				[$MAKE_SUCCESS] )
					echo -e "$YELLOW BUILD SUCCESS! $build_type All Images Identified in $OUTPUT_TARGET_PATH path $ENDCOLOR";;
				* )
					echo -e "$RED ERROR! $build_type BUILD FAILED! Retry Build ($retry_left retry left)$ENDCOLOR";
					echo -e "$RED ERROR! Build Result! Make-$build_result, Img-$image_verify_result, Log-$log_verify_result $ENDCOLOR";
					msg="Confirm for Retry the Build"
					confirm_to_continue=$ERROR_SUCCESS
					wait_for_user $msg
					confirm_to_continue=$?
					if [[ "$confirm_to_continue" != "$ERROR_SUCCESS" ]]
					then
						break;
					fi
			esac
		fi

		retry_left=$(expr $retry_left - 1)
		if test $retry_left -lt 0
		then
			break;
		fi
	done

	if [[ $build_success != $MAKE_SUCCESS ]];
	then
		result=$ERROR_FAILURE
	fi

	return $result
}

#Clean the Build
build_clean()
{
	isprint=$1 #Get the Print Command

	setup_env $isprint

	$isprint \
	make clean

	$isprint \
	rm -rf $OUTPUT_DIR
}

#Create BackUp
create_backup()
{
	BACKUP_FILE_NAME="$BUILDNAME""_"$(date +"%y%m%d_%H%M%S").tar.gz

	isprint=$1 #Get the Print Command

	if [ -z "$isprint" ]
	then
		echo "" > $BUILDROOT/$BACKUP_DIR_CONFIG_FILE
		for omit_dir in $BACKUP_DIR_CONFIG; do
			$isprint \
			echo $omit_dir >> $BUILDROOT/$BACKUP_DIR_CONFIG_FILE
		done

		cd $SDK_ROOT

		cat $BUILDROOT/$BACKUP_DIR_CONFIG_FILE

		tar --checkpoint="$CHECK_POINT_VAL" \
			-zcf "$BUILDROOT/$BACKUP_FILE_NAME" \
			--exclude-from="$BUILDROOT/$BACKUP_DIR_CONFIG_FILE" \
			.

	else
		echo -e $GREEN" This step will take the back up for the entire build \n"\
			"Location : $BUILDROOT/$BUILDNAME""_mmddyy_hhmmss\n"\
			$ENDCOLOR
	fi
}

#Download from CLO
download_clo_release()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS
	force_download=$2

	check_for_clo_rel_exists $isprint
	do_download=$?

	$isprint \
	cd $WORKDIR

	if [[ $do_download -eq $ERROR_SUCCESS ]] || [[ $force_download -eq 1 ]]
	then
		$isprint \
		rm -rf $CLO_CLONE_DIR

		$isprint \
		git clone -n --filter=tree:0 $CLO_REL_PATH $CLO_REL_DIR_NAME

		$isprint \
		cd $CLO_CLONE_DIR

		$isprint \
		git sparse-checkout set --no-cone $CLO_REL_TARGET_DIR_NAME/$CLO_REL_VERSION

		$isprint \
		git checkout
	fi

	$isprint \
	ls -al $WORKDIR $CLO_REL_DIR

	return $result
}

#Final Set of Instructions and Messages
post_setup()
{
	isprint=$1 #Get the Print Command
	local result=$ERROR_SUCCESS

	if [ $RETAIN_OLD_BIN -ne 1 ]
	then
		BIN_FILE_NAME="$BUILDNAME"
	else
		BIN_FILE_NAME="$BUILDNAME""_""$(date +"%y%m%d_%H%M%S")"
	fi

	$isprint \
	cd $SDK_ROOT

	$isprint \
	cp $OUTPUT_DIR/dist/$SUPER_IMAGE_NAME $OUTPUT_TARGET_PATH

	$isprint \
	unzip -qoj  $OUTPUT_DIR/dist/sdm660_64-target_files-eng.$USER.zip IMAGES/$VB_META_IMAGE_NAME -d $OUTPUT_TARGET_PATH

	$isprint \
	unzip -qoj  $OUTPUT_DIR/dist/sdm660_64-target_files-eng.$USER.zip IMAGES/$VB_META_SYSTEM_IMAGE_NAME -d $OUTPUT_TARGET_PATH


	setup_bin_dir $isprint

	$isprint \
	cd $BIN_DIR

	binary_list=$(get_build_item_details "" "" $GET_ITEM_IMAGENAME)
	for img in $binary_list; do
		$isprint \
		cp $OUTPUT_TARGET_PATH/$img $BIN_DIR
	done

	$isprint \
	cp $CLO_REL_DIR/$FLASHING_SCRIPT $BIN_DIR

	$isprint \
	zip -r $BIN_FILE_NAME.zip . -i $binary_list $FLASHING_SCRIPT

	$isprint \
	zipinfo $BIN_DIR/$BIN_FILE_NAME.zip

	$isprint \
	ls -al $BIN_DIR

	return $result
}

#Final Set of Instructions and Messages
exit_step()
{
	cd $WORKDIR
}

#===============================================================================#
#Utility Functions
#===============================================================================#
color()
{
	echo -e $1
}

print_line()
{
	echo -e $RED"==============================================================================================================================================================="$ENDCOLOR
}

print_limits()
{
	intent=$1
	msg=$2
	print_line
	echo -e $YELLOW $@
	print_line
}

wait_for_user()
{
	msg=$@
	color $MAGENTA $UNDER
	# echo $msg
	local confirm_to_continue
	confirm_to_continue=$ERROR_SUCCESS
	if [ "$ENABLE_PROMTS" = "$SET_VALUE" ]
	then
		check_for_confirmation $msg
		confirm_to_continue=$?
		# read -p "Enter to Continue"
	fi
	color $ENDCOLOR

	return $confirm_to_continue
}

exectute_cmd()
{
	local confirm_to_continue
	local result=$ERROR_SUCCESS
	confirm_to_continue=$ERROR_SUCCESS
	call_fn=$1
	display_msg=$2
	print_limits $display_msg Commands
	color $CYAN
	$call_fn "echo" $3
	if [ "$PRINT_ONLY" != "$SET_VALUE" ]
	then
		print_line
		wait_for_user "Please verify the instructions and proceed with execution"
		confirm_to_continue=$?
		if [ $confirm_to_continue -eq $ERROR_SUCCESS ]
		then
			print_limits $display_msg Begin
			$call_fn "" $3
			result=$?
			print_limits $display_msg End
		fi
	fi
	return $result
}

#===============================================================================#
#Color Defines
#===============================================================================#
set_colors()
{
	UNDER='\e[4m'
	RED='\e[31;1m'
	GREEN='\e[32;1m'
	YELLOW='\e[33;1m'
	BLUE='\e[34;1m'
	MAGENTA='\e[35;1m'
	CYAN='\e[36;1m'
	WHITE='\e[37;1m'
	ENDCOLOR='\e[0m'
}

#===============================================================================#
#Build Routine
#===============================================================================#
setup_and_build()
{
	exectute_cmd start_instructions "Instructions" &&
	exectute_cmd install_tools "Setting Up the Tools Required" &&
	exectute_cmd setup_git "Setting Up GIT" &&
	exectute_cmd download_clo_release "Download Scripts and Patches from CLO" "$FORCE_DOWNLOAD" &&
	exectute_cmd setup_root "Setting Up the Root Folder Stucture" &&
	exectute_cmd extract_patches3 "Extracting Patches3" &&
	exectute_cmd setup_sdk "Setting Up SDK" &&
	exectute_cmd setup_sdk_part2 "Applying Patches1" &&
	exectute_cmd apply_patches "Applying the Patches2" "$PATCH2_DIR_NAME" &&
	exectute_cmd apply_patches "Applying the Patches3" "$PATCH3_DIR_NAME" &&
	exectute_cmd build_and_verify "Building All Images" &&
	exectute_cmd post_setup "Finishing Build Setup"
}

#===============================================================================#
# Update Functions
#===============================================================================#
update_build_name()
{
	local value
	echo "Current Value :" $BUILDNAME
	read -r -p "Enter new Build Name : " value
	BUILDNAME=$value
	echo "New Value :" $BUILDNAME
	_reconfig
}

update_num_make_jobs()
{
	local value
	echo "Current Value :" $MAKE_JOB_THREADS
	read -r -p "Enter new Num Make Jobs : " value
	MAKE_JOB_THREADS=$value
	echo "New Value :" $MAKE_JOB_THREADS
	_reconfig
}

update_num_repo_jobs()
{
	local value
	echo "Current Value :" $REPO_JOB_THREADS
	read -r -p "Enter new Num Repo Jobs : " value
	REPO_JOB_THREADS=$value
	echo "New Value :" $REPO_JOB_THREADS
	_reconfig
}

update_enable_disable_promts()
{
	local value
	echo "Current Value :" $ENABLE_PROMTS
	read -r -p "Enable/Disable Promts(0/1) : " value
	ENABLE_PROMTS=$value
	echo "New Value :" $ENABLE_PROMTS
	_reconfig
}

update_enable_print_only()
{
	local value
	echo "Current Value :" $PRINT_ONLY
	read -r -p "Enable Print Only (0/1) : " value
	PRINT_ONLY=$value
	echo "New Value :" $PRINT_ONLY
	_reconfig
}

update_no_tool_chain_update()
{
	local value
	echo "Current Value :" $NO_TOOL_CHAIN_UPDATE
	read -r -p "Enable/Disable Tool Chain Update(0/1) : " value
	NO_TOOL_CHAIN_UPDATE=$value
	echo "New Value :" $NO_TOOL_CHAIN_UPDATE
	_reconfig
}

update_git_config_name()
{
	local value
	echo "Current Value :" $GIT_USER_NAME
	read -r -p "Enter Git User Name : " value
	GIT_USER_NAME=$value
	echo "New Value :" $GIT_USER_NAME
	_reconfig
}

update_git_config_email()
{
	local value
	echo "Current Value :" $GIT_USER_EMAIL
	read -r -p "Enter Git User Email : " value
	GIT_USER_EMAIL=$value
	echo "New Value :" $GIT_USER_EMAIL
	_reconfig
}

update_force_download()
{
	local value
	echo "Current Value :" $FORCE_DOWNLOAD
	read -r -p "Enable/Disable Force Download (0/1) : " value
	FORCE_DOWNLOAD=$value
	echo "New Value :" $FORCE_DOWNLOAD
	_reconfig
}

update_clo_dir_name()
{
	local value
	echo "Current Value :" $CLO_REL_DIR_NAME
	read -r -p "Enter CLO Directory name : " value
	CLO_REL_DIR_NAME=$value
	echo "New Value :" $CLO_REL_DIR_NAME
	_reconfig
}

update_clo_target_dir_name()
{
	local value
	echo "Current Value :" $CLO_REL_TARGET_DIR_NAME
	read -r -p "Enter CLO Target Directory name : " value
	CLO_REL_TARGET_DIR_NAME=$value
	echo "New Value :" $CLO_REL_TARGET_DIR_NAME
	_reconfig
}

update_clo_version_name()
{
	local value
	echo "Current Value :" $CLO_REL_VERSION
	read -r -p "Enter CLO Version name : " value
	CLO_REL_VERSION=$value
	echo "New Value :" $CLO_REL_VERSION
	_reconfig
}

update_clo_release_path()
{
	local value
	echo "Current Value :" $CLO_REL_PATH
	read -r -p "Enter CLO Release Path : " value
	CLO_REL_PATH=$value
	echo "New Value :" $CLO_REL_PATH
	_reconfig
}

update_sdk_file_name()
{
	local value
	echo "Current Value :" $SDK_FILE
	read -r -p "Enter SDK File name : " value
	SDK_FILE=$value
	echo "New Value :" $SDK_FILE
	_reconfig
}

update_patches3_file_name()
{
	local value
	echo "Current Value :" $PATCHES3_FILE
	read -r -p "Enter Patches3 File name : " value
	PATCHES3_FILE=$value
	echo "New Value :" $PATCHES3_FILE
	_reconfig
}

update_sdk_directory_name()
{
	local value
	echo "Current Value :" $SDK_DIR
	read -r -p "Enter SDK Directory name : " value
	SDK_DIR=$value
	echo "New Value :" $SDK_DIR
	_reconfig
}

update_local_repo_name()
{
	local value
	echo "Current Value :" $LOCAL_REPO_FILE
	read -r -p "Enter Local Repo name : " value
	LOCAL_REPO_FILE=$value
	echo "New Value :" $LOCAL_REPO_FILE
	_reconfig
}

update_si_clo_release_tag()
{
	local value
	echo "Current Value :" $CLO_SI_REL_TAG
	read -r -p "Enter CLO Release Tag xml name : " value
	CLO_SI_REL_TAG=$value
	echo "New Value :" $CLO_SI_REL_TAG
	_reconfig
}

update_si_clo_url()
{
	local value
	echo "Current Value :" $CLO_URL
	read -r -p "Enter CLO URL : " value
	CLO_URL=$value
	echo "New Value :" $CLO_URL
	_reconfig
}

update_si_clo_manifest_path()
{
	local value
	echo "Current Value :" $CLO_MANIFEST
	read -r -p "Enter CLO Manifest Path : " value
	CLO_MANIFEST=$value
	echo "New Value :" $CLO_MANIFEST
	_reconfig
}

update_si_clo_repo_path()
{
	local value
	echo "Current Value :" $CLO_REPO
	read -r -p "Enter CLO Repo Path : " value
	CLO_REPO=$value
	echo "New Value :" $CLO_REPO
	_reconfig
}

update_use_local()
{
	local value
	echo "Current Value :" $USE_LOCAL
	read -r -p "Enable/Disable Local Repo(0/1) : " value
	USE_LOCAL=$value
	echo "New Value :" $USE_LOCAL
	_reconfig
}

update_retain_old_bin()
{
	local value
	echo "Current Value :" $RETAIN_OLD_BIN
	read -r -p "Enable/Disable Retain Final Image (0/1) : " value
	RETAIN_OLD_BIN=$value
	echo "New Value :" $RETAIN_OLD_BIN
	_reconfig
}

update_error_min_retry()
{
	local value
	echo "Current Value :" $ERROR_MIN_RETRY
	read -r -p "Enter Number of Minimum Retry  : " value
	ERROR_MIN_RETRY=$value
	echo "New Value :" $ERROR_MIN_RETRY
	_reconfig
}

reset_config()
{
	msg="About to reset config"
	local confirm_to_continue
	confirm_to_continue=$ERROR_SUCCESS
	check_for_confirmation $msg
	confirm_to_continue=$?
	if [ $confirm_to_continue -eq $ERROR_SUCCESS ]
	then
		_config
		_reconfig
	fi
}

#===============================================================================#
# Menu Config
#===============================================================================#
show_main_menu()
{
	display_msg="$BUILDNAME MAIN MENU"
	print_limits $display_msg
	echo "  1. Setup New Build"
	echo "  2. Print All Configs"
	echo "  3. Make All"
	echo "  4. Clean All"
	echo "  5. Prepare Final Images"
	echo "  6. Create a Backup"
	echo "  7. Download from CLO"
	echo "  8. Make ABoot Image"
	echo "  9. Make Kernel Image"
	echo " 10. Make System Image"
	echo " 11. Make User Data Image"
	echo " 12. Make Vendor Image"
	echo " 99. Show Config Menu"
	echo "100. Quit"
}

#===============================================================================#
# Config Menu
#===============================================================================#
show_config_menu()
{
	display_msg="$BUILDNAME CONFIGURATION MENU"
	print_limits $display_msg
	echo "  0. Main Menu"
	echo "  1. Print All Configs"
	echo "  2. Reset All Configuration"
	echo "  3. Update Build Name"
	echo "  4. Enable/Disable Promts"
	echo "  5. Enable Print Only"
	echo "  6. Update Force Download Option"
	echo "  7. Update Git Config Email"
	echo "  8. Update Git Config Name"
	echo "  9. Update Num Make Jobs"
	echo " 10. Update USE LOCAL"
	echo " 11. Update Local Repo Name"
	echo " 12. Retain Previous Images"
	echo " 13. Update SDK File Name"
	echo " 14. Update SDK Directory Name"
	echo " 15. Update CLO Directory Name"
	echo " 16. Update CLO Target Directory Name"
	echo " 17. Update CLO Release Path"
	echo " 18. Update CLO Version Name"
	echo " 19. Update Num Minimum Retry "
	echo " 20. Enable/Disable Tool Chain update"
	echo " 21. Update Patches3 File Name"
	echo " 22. Update Num Repo Jobs"
	echo " 23. Update SI CLO Manifest Path"
	echo " 24. Update SI CLO Release Tag"
	echo " 25. Update SI CLO Repo Path"
	echo " 26. Update SI CLO URL"
	echo "100. Quit"
}

execute_functions()
{
	local curr_menu_id
	selection=$1
	curr_menu_id=$2
	case $curr_menu_id in
		$MENU_MAIN)
		case $selection in
			1 ) setup_and_build;;
			2 ) print_config;;
			3 ) exectute_cmd build_and_verify "Make All Images"  ;;
			4 ) exectute_cmd build_clean "Clean the Entire Build";;
			5 ) exectute_cmd post_setup "Prepare Final Images";;
			6 ) exectute_cmd create_backup "Create a Backup";;
			7 ) exectute_cmd download_clo_release "Download from CLO" 1;;
			8 ) exectute_cmd build_and_verify "Building ABoot Image" $ABOOT_BUILD_TYPE ;;
			9 ) exectute_cmd build_and_verify "Building Kernel Image" $BOOT_BUILD_TYPE ;;
			10) exectute_cmd build_and_verify "Building System Image" $SYSTEM_BUILD_TYPE ;;
			11) exectute_cmd build_and_verify "Building User Data Image" $USERDATA_BUILD_TYPE ;;
			12) exectute_cmd build_and_verify "Building Vendor Image" $VENDOR_BUILD_TYPE ;;
			99) curr_menu_id=$MENU_CONFIG;;
			* ) echo "Incorrect Option Selected" && sleep 1;;
			esac
		;;
		$MENU_CONFIG) case $selection in
			 0) curr_menu_id=$MENU_MAIN;;
			 1) print_config;;
			 2) reset_config;;
			 3) update_build_name;;
			 4) update_enable_disable_promts;;
			 5) update_enable_print_only;;
			 6) update_force_download;;
			 7) update_git_config_email;;
			 8) update_git_config_name;;
			 9) update_num_make_jobs;;
			10) update_use_local;;
			11) update_local_repo_name;;
			12) update_retain_old_bin;;
			13) update_sdk_file_name;;
			14) update_sdk_directory_name;;
			15) update_clo_dir_name;;
			16) update_clo_target_dir_name;;
			17) update_clo_release_path;;
			18) update_clo_version_name;;
			19) update_error_min_retry;;
			20) update_no_tool_chain_update;;
			21) update_patches3_file_name;;
			22) update_num_repo_jobs;;
			23) update_si_clo_manifest_path;;
			24) update_si_clo_release_tag;;
			25) update_si_clo_repo_path;;
			26) update_si_clo_url;;
			* ) echo "Incorrect Option Selected" && sleep 1;;
			esac
		;;
	esac
	return $curr_menu_id;
}

read_selection()
{
	local selection
	end_val=$1
	exit_val=$2
	read -r -p "Enter Selection : " selection
	if ([ -z "$selection" ])
	then
		selection=-1
	fi
	if((($selection<0 || $selection>$end_val) && ($selection!=$exit_val)))
	then
		selection=-1
	fi
	return $selection;
}

#===============================================================================#
# Main Function
#===============================================================================#
_main()
{
	_config
	_reconfig
	set_colors
	num_selections=100
	exit_val=100
	curr_menu_id=$MENU_MAIN
	user_input=$1

	if [[ ! -z $user_input ]]
	then
		ENABLE_PROMTS=$CLEAR_VALUE
		execute_functions $user_input $MENU_MAIN
		_config
		_reconfig
		return
	fi

	while true
	do
		case $curr_menu_id in
			$MENU_MAIN) show_main_menu;;
			$MENU_CONFIG) show_config_menu;;
		esac
		read_selection $num_selections $exit_val
		selection=$?
		if [ $selection -eq $exit_val ]
		then
			echo "Exiting!!" && sleep 2
			break;
		fi
		execute_functions $selection $curr_menu_id
		curr_menu_id=$?
	done
	exit_step
}
_main $1
