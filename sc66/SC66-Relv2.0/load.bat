@echo off
TITLE [LOAD] (%cd%)
SET /A ARGS_COUNT=0
SET adbcommand=adb
FOR %%A in (%*) DO SET /A ARGS_COUNT+=1
if %ARGS_COUNT% == 1 SET adbcommand=%adbcommand% -s %1
echo %adbcommand%
@echo on
%adbcommand% reboot bootloader
pause
fastboot flash boot boot.img
fastboot flash userdata userdata.img
fastboot flash abl abl.elf
fastboot flash dtbo dtbo.img
fastboot flash vbmeta vbmeta.img
fastboot flash vbmeta_system vbmeta_system.img
fastboot flash super super.img
@echo off
REM fastboot flash xbl xbl.elf
@echo on
pause
fastboot reboot

