#!/bin/bash
UNDER='\e[4m'
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
BLUE='\e[34;1m'
MAGENTA='\e[35;1m'
CYAN='\e[36;1m'
WHITE='\e[37;1m'
ENDCOLOR='\e[0m'
WORKDIR=`pwd`
BUILDNAME="Android_SDK_SC66"
BUILDNAME2="Android_SDK_SC66_new"
BUILDSRC="Android_SDK_SC66_SRC"
BUILDROOT="$WORKDIR/../$BUILDNAME"
CAFTAG="LA.UM.8.2.1.r1-04200-sdm660.0.xml"
QUECTEL_PATCH_FILE="SC66_Android10.0_Quectel_SDK_r008_20200604.tar.gz"
PATCH_DIR="$WORKDIR/patches"
PATCH_DIR2="$WORKDIR/patches2"
PATCH_DIR3="$WORKDIR/patches3"
QDN_PATCH_FILENAME="sc66-qdn_relv0.1"
PATCH_TYPE="."
PATCH_TYPE2="."

function setup_folders() {
if [ -e $BUILDROOT ]
then
	cd $BUILDROOT
else
	mkdir $BUILDROOT
	cd $BUILDROOT
fi
}
function update_paths() {
if [ "$1" = "new" ]
then
	PATCH_TYPE=$PATCH_TYPE2
	BUILDNAME=$BUILDNAME2
  BUILDROOT="$WORKDIR/../$BUILDNAME"
  PATCH_DIR2="$PATCH_DIR2/$PATCH_TYPE2"
  PATCH_DIR3="$PATCH_DIR3/$PATCH_TYPE2"
else
  PATCH_DIR2="$PATCH_DIR2/$PATCH_TYPE"
  PATCH_DIR3="$PATCH_DIR3/$PATCH_TYPE"
fi
echo Build Path = $BUILDNAME
}

function download_CAF_CODE () {
echo "download_CAF_CODE"
# Do repo sanity test
if [ $? -eq 0 ]
then
	echo "Downloading code please wait.."
	repo init -u https://source.codeaurora.org/quic/la/platform/manifest.git -b release -m ${CAFTAG} --repo-url=git://codeaurora.org/tools/repo.git --repo-branch=caf-stable
	repo sync -cj16 -q --no-tags -f --no-clone-bundle
	if [ $? -eq 0 ]
	then
		echo -e "$GREEN Downloading done..$ENDCOLOR"
	else
		echo -e "$RED!!!Error Downloading code!!!$ENDCOLOR"
	fi
else
	echo "repo tool problem, make sure you have setup your build environment"
	echo "1) http://source.android.com/source/initializing.html"
	echo "2) http://source.android.com/source/downloading.html (Installing Repo Section Only)"
	exit -1
fi
}

#copy Source Locally
copy_source()
{
	cd -
	cp -rf $BUILDSRC/./ $BUILDROOT/
	cd -
}

# Function to apply Quectel patches to CAF code
apply_android_patches()
{

	echo "Applying patches ..."
	if [ ! -e $PATCH_DIR ]
	then
		echo -e "$RED $PATCH_DIR : Not Found $ENDCOLOR"
	fi
	cd $PATCH_DIR
	tar -xvf $QUECTEL_PATCH_FILE -C $BUILDROOT
}

#Function to extract QDN patches
function extract_qdn_patches()
{
	cd $WORKDIR
	unzip $QDN_PATCH_FILENAME -d temp_folder
	cp -rf temp_folder/*/patches3 $PATCH_DIR3
	rm -rf temp_folder
}

# Function to commit Default Changes
commit_initial_changes()
{
	echo commit initial changes
	git init
	git add .
	git commit -m InitialCommit -s
}

# Function to apply patches to CAF code(2nd Set)
apply_android_patches2()
{

	echo "Applying patches 2 ..." $1
	PATCH_DIR=$1
	if [ ! -e $PATCH_DIR ]
	then
		echo -e "$RED $PATCH_DIR : Not Found $ENDCOLOR"
    exit
	fi

	cd $PATCH_DIR
	patch_root_dir="$PATCH_DIR"
	android_patch_list=$( find . -type f -name '*.patch' | sed -r 's|/[^/]+$||' |sort -u) &&
	for android_patch in $android_patch_list; do
		android_project=$android_patch
		echo -e "$YELLOW  Performing InitialCommit on $android_project ... $ENDCOLOR"
		cd $BUILDROOT/$android_project
		if [ $? -ne 0 ]; then
			echo -e "$RED $android_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
			mkdir -p $BUILDROOT/$android_project
			cd $BUILDROOT/$android_project
		fi
		commit_initial_changes
		pwd
	done
	cd $PATCH_DIR
	patch_root_dir="$PATCH_DIR"
	android_patch_list=$(find . -type f -name "*.patch" | sort) &&
	for android_patch in $android_patch_list; do
		android_project=$(dirname $android_patch)
		echo -e "$YELLOW   applying patches on $android_project ... $ENDCOLOR"
		cd $BUILDROOT/$android_project
		if [ $? -ne 0 ]; then
			echo -e "$RED $android_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
			exit 1
		fi
		pwd
		echo $patch_root_dir
		echo $android_patch
		git am $patch_root_dir/$android_patch
	done
}

# Function to apply patches to CAF code(2nd Set)
build()
{
	cd $BUILDROOT
	source build/envsetup.sh
	lunch sdm660_64-userdebug
	./build.sh dist -j16
}

# Function to apply patches to CAF code(2nd Set)
postbuild()
{
	cd $BUILDROOT
	cp out/dist/super.img out/target/product/sdm660_64/
	unzip -qoj  out/dist/sdm660_64-target_files-eng.qil01.zip IMAGES/vbmeta.img -d out/target/product/sdm660_64/
	unzip -qoj  out/dist/sdm660_64-target_files-eng.qil01.zip IMAGES/vbmeta_system.img -d out/target/product/sdm660_64/
}

echo "Begin"

echo check_program tar repo git patch

#Updates Path for 3.5 inch LCD option use parameter as 3p5
update_paths $1

#Setup Folders
setup_folders

#1 Download code
download_CAF_CODE
#copy_source
#2 Apply Patches from Quectel
apply_android_patches
#3 Apply Patches for Carrier Board
apply_android_patches2 $PATCH_DIR2
#4 Apply QDN Patches
extract_qdn_patches
apply_android_patches2 $PATCH_DIR3
#5 Build
build
#6 Post Build
postbuild
echo "End"
