#!/bin/bash
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
BLUE='\e[34;1m'
MAGENTA='\e[35;1m'
CYAN='\e[36;1m'
WHITE='\e[37;1m'
ENDCOLOR='\e[0m'
NUM_LIST=(1 2)
WORKDIR=`pwd`
BUILDNAME="EC25_SDK"
BUILDROOT="$WORKDIR/../$BUILDNAME"
PATCH_DIR="$WORKDIR/patches"
PATCH_DIR2="$WORKDIR/patches2"
PATCH_DIR3="$WORKDIR/patches3"
QUECTEL_SDK_FILE="EC25_sdk.tar.bz2"
SDK_PATH="ql-ol-sdk"
SDKROOT="$BUILDROOT/$SDK_PATH"
QDN_PATCH_FILENAME="ec25-qdn_relv1.0"
PATCH_DIR_OS="$WORKDIR/patches_os"
OS_FILES_PATH=(os/ql-ol-kernel/drivers/power/ os/ql-ol-kernel/include/linux/power/)
OS_LINKS=(https://android.googlesource.com/kernel/tegra/+archive/b445e5296764d18861a6450f6851f25b9ca59dee/drivers/power.tar.gz 
         https://android.googlesource.com/kernel/tegra/+archive/b445e5296764d18861a6450f6851f25b9ca59dee/include/linux/power.tar.gz)
OS_FILES_NAME=(power.tar.gz power_includes.tar.gz)
OS_FILES_LIST=("bq27441_battery.c battery-charger-gauge-comm.c" "bq27441_battery.h battery-charger-gauge-comm.h")

# Setup SDK Folder
setup_folder()
{
    echo Begin
    if [ -e $BUILDROOT ]
    then
	    cd $BUILDROOT
    else 
	    mkdir $BUILDROOT
	    cd $BUILDROOT
    fi
}

# Extract SDK
setup_sdk()
{
  cd $PATCH_DIR
  echo Extract SDK Files to $BUILDROOT
  tar -xf $QUECTEL_SDK_FILE --checkpoint=10000 -C $BUILDROOT
}

# Function to download CAF patches
download_caf_mirror_patches()
{
  echo "Dowloading CAF patches ..."

  if [ -e $PATCH_DIR_CAF ]
  then
    rm -rf $PATCH_DIR_CAF
  else
    mkdir $PATCH_DIR_CAF
  fi
  git clone $CAF_PATCHES_CLONE_LINK $PATCH_DIR_CAF
}

# Function to apply CAF patches to CAF code
download_apply_os_files()
{
        echo "Dowloading OS Files ..."

        if [ -e $PATCH_DIR_OS ]
        then
                rm -rf $PATCH_DIR_OS
        fi

        for i in "${NUM_LIST[@]}";
        do
            idx=$((i-1))        
            mkdir -p $PATCH_DIR_OS/${OS_FILES_PATH[idx]}
            wget -t 0 ${OS_LINKS[idx]} -O $PATCH_DIR_OS/${OS_FILES_NAME[idx]}
            tar -xf $PATCH_DIR_OS/${OS_FILES_NAME[idx]} -C $PATCH_DIR_OS/${OS_FILES_PATH[idx]} ${OS_FILES_LIST[idx]}
        done
        cp -rf $PATCH_DIR_OS/os/./ $SDKROOT/

}

# Initialize Toolchain & configure kernel_menuconfig
initialize_toolchain()
{
  cd $SDKROOT
  echo Initializing SDK at $SDKROOT
  . ql-ol-crosstool/ql-ol-crosstool-env-init
}

# Function to initialize git
initialize_git()
{
  cd $SDKROOT
  git init .
  echo Adding All files to Git
  git add .
  echo Initial Commit
  git commit -q -m "Initial Commit"
  git repack -d -l
}

#Function to extract QDN patches
function extract_qdn_patches()
{
	cd $WORKDIR
	unzip $QDN_PATCH_FILENAME -d temp_folder
	cp -rf temp_folder/*/patches3 $PATCH_DIR3
	rm -rf temp_folder
}

# Function to apply patches
apply_ec25_patches()
{
	PATCH_DIR=$1
  echo Applying patches from $PATCH_DIR
	cd $PATCH_DIR
	patch_list=$(find . -type f -name "*.patch" | sort) &&
  cd $SDKROOT
	for patch in $patch_list; do
		echo $PATCH_DIR/$patch
		git am $PATCH_DIR/$patch
	done
}

# Build
build()
{
  cd $SDKROOT
  echo Starting Building, Might take few minutes to complete
  make kernel_menuconfig
  make -j16
}

echo -e "$BLUE EC25 Build Process Begins Here, Might take few hours to complete Wait Patiently $ENDCOLOR"

# Setup SDK Folder
setup_folder
# Extract SDK
setup_sdk
# Initialize Toolchain & configure kernel_menuconfig
initialize_toolchain
# Apply OS Files
download_apply_os_files
# Function to initialize git
initialize_git
# Function to apply patches
apply_ec25_patches $PATCH_DIR2
# Function to apply patches
extract_qdn_patches
apply_ec25_patches $PATCH_DIR3
# Build
build

echo -e "$GREEN ### Make Completed Succesfully ### $ENDCOLOR"

echo End
