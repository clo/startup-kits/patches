#!/bin/bash

WORK_DIR=`pwd`
V1_DIR_CHK="./EC20_releasetools/v1"
V2_DIR_CHK="./EC20_releasetools/v2"
V1_DIR="$WORK_DIR/EC20_releasetools/v1"
V2_DIR="$WORK_DIR/EC20_releasetools/v2"
VERA_DIR="$WORK_DIR/verA"
VERZ_DIR="$WORK_DIR/verZ"
ONE=1
DELTAPKGCOUNT=1
SDKPATH="NIL"
UPGRADEPATH="NIL"
TAR_UPG_PATH="NIL"
SDKBOOTIMGPATH="NIL"

echo "Delta Package Script Starts... "

#echo $UPGRADEPATH
#echo $SDKPATH
#echo $TAR_UPG_PATH


echo "Checking verA & verZ folder structure..."

UPGRADECHK=$(find -iname *upgrade)
for one in $UPGRADECHK;do
#   DIRONE=$(dirname $one)
#   echo $DIRONE
#   echo $one
    UPGRADEPATH=$one
done

SDKCHK=$(find -iname *ql-ol-rootfs)
for one in $SDKCHK;do
#    DIRONE=$(dirname $one)
#    echo $DIRONE
#    echo $one
    SDKPATH=$one
done

cd $SDKPATH/../target/
SDKBOOTIMGCHK=$(find -iname mdm9607-perf-boot.img)
for one in $SDKBOOTIMGCHK;do
    SDKBOOTIMGPATH=$one
    echo $one
done

cd $WORK_DIR

echo "Checking & Clearing Targetfiles from v1 & v2....."

if [ -d "$V1_DIR" ]; then
  echo "$V1_DIR exists..."
else
  mkdir $V1_DIR
  echo "$V1_DIR Created..."
fi

if [ -d "$V2_DIR" ]; then
  echo "$V2_DIR exists..."
else
  mkdir $V2_DIR
  echo "$V2_DIR Created..."
fi

TARGETFILES=$(find -iname *targetfiles.zip*)
for one in $TARGETFILES; do
	DIRONE=$(dirname $one)
  if [ $DIRONE == $V1_DIR_CHK ]; then
    echo "Found 'targetfiles.zip' in v1 folder, Removing it..."
	  rm -rf $one
  fi	
  if [ $DIRONE == $V2_DIR_CHK ]; then
    echo "Found 'targetfiles.zip' in v2 folder, Removing it..."
	  rm -rf $one
  fi
  if [ $DIRONE == $UPGRADEPATH ]; then
    echo "Found 'targetfiles.zip' in Upgarde folder..."
    TAR_UPG_PATH=$one
  fi
done
  rm -rf $V1_DIR/*
  rm -rf $V2_DIR/*

echo $UPGRADEPATH
echo $SDKPATH
echo $TAR_UPG_PATH
echo $SDKBOOTIMGPATH

delta_package ()
{
echo "Preparing Delta Package..."
cp -rf $TAR_UPG_PATH $V1_DIR
cp -rf $TAR_UPG_PATH $V2_DIR
echo "10%..."
unzip -d $V2_DIR/target $V2_DIR/targetfiles.zip
echo "20%..."
cp -rf $SDKPATH/../target/mdm9607-perf-boot.img $V2_DIR/target/BOOTABLE_IMAGES/boot.img
echo "30%..."
rm -rf $V2_DIR/targetfiles.zip
mv $V2_DIR/target/SYSTEM/firmware $V2_DIR/target/
echo "40%..."
rm -rf $V2_DIR/target/SYSTEM/*
cp -rf $SDKPATH/* $V2_DIR/target/SYSTEM
echo "50%..."
rm -rf $V2_DIR/target/SYSTEM/firmware
cp -rf $V2_DIR/target/firmware $V2_DIR/target/SYSTEM/
echo "60%..."
rm -rf $V2_DIR/target/firmware
echo "70%..."
cd $V2_DIR/target/
zip -qry0 ../targetfiles.zip ./*
cd $WORK_DIR
rm -rf $V2_DIR/target
cd $WORK_DIR/EC20_releasetools/
./update_gen.sh a
echo "100%..."
echo "Delta package is Ready..."
}

#SDKPATH="NIL"

if [ $UPGRADEPATH != "NIL" ] && [ $SDKPATH != "NIL" ] && [ $TAR_UPG_PATH != "NIL" ] && [ $SDKBOOTIMGPATH != "NIL" ]; then
  delta_package
else
  echo "### DFOTA Failed : DFOTA Folder Structure is wrong"
  echo "### CHECK : targetfiles.zip/upgarde folder available in verA Folder"
  echo "### CHECK : Compiled SDK placed in verZ Folder"
fi
echo "End"