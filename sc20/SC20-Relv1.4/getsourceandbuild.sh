#!/bin/bash
UNDER='\e[4m'
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
BLUE='\e[34;1m'
MAGENTA='\e[35;1m'
CYAN='\e[36;1m'
WHITE='\e[37;1m'
ENDCOLOR='\e[0m'
WORKDIR=`pwd`
BUILDNAME="Android_sc20_lcd_5in"
BUILDNAME2="Android_sc20_lcd_3p5in"
BUILDSRC="Android_sc20_src"
BUILDROOT="$WORKDIR/../$BUILDNAME"
CAFTAG="LA.BR.1.2.9-04610-8x09.0.xml"
QUECTEL_PATCH_FILE="SC20_Android7.1.2_Quectel_SDK_r00049_20181106.tar.gz"
OS_FILES_LINK="https://android.googlesource.com/kernel/msm.git/+archive/aac1b45b3193454d71f02f73ac46d7f45ed0e6e4/drivers/input/touchscreen/ft8716.tar.gz"
OS_FILES_NAME="ft3668.tar.gz"
OS_FILES_PATH="os/kernel/drivers/input/touchscreen/ft3668/"
OS_FILES_LIST="FT_Upgrade_App.i ft_gesture_lib.h focaltech_core.c focaltech_core.h focaltech_ctl.c focaltech_ex_fun.c focaltech_flash.c focaltech_gesture.c Makefile"
PATCH_DIR="$WORKDIR/patches"
PATCH_DIR2="$WORKDIR/patches2"
PATCH_DIR3="$WORKDIR/patches3"
PATCH_DIR_OS="$WORKDIR/patches_os"
QDN_PATCH_FILENAME="sc20-qdn_relv1.0.zip"
PATCH_TYPE="lcd5"
PATCH_TYPE2="lcd3p5"

function setup_folders() {
if [ -e $BUILDROOT ]
then
	cd $BUILDROOT
else
	mkdir $BUILDROOT
	cd $BUILDROOT
fi
}
function update_paths() {
if [ "$1" = "3p5" ]
then
	PATCH_TYPE=$PATCH_TYPE2
	BUILDNAME=$BUILDNAME2
  BUILDROOT="$WORKDIR/../$BUILDNAME"
  PATCH_DIR2="$PATCH_DIR2/$PATCH_TYPE2"
  PATCH_DIR3="$PATCH_DIR3/$PATCH_TYPE2"
else
  PATCH_DIR2="$PATCH_DIR2/$PATCH_TYPE"
  PATCH_DIR3="$PATCH_DIR3/$PATCH_TYPE"
fi
echo Build Path = $BUILDNAME
}

function download_CAF_CODE () {
echo "download_CAF_CODE"
# Do repo sanity test
if [ $? -eq 0 ]
then
	echo "Downloading code please wait.."
	repo init --depth 1 -u git://codeaurora.org/platform/manifest.git -b release -m ${CAFTAG} --repo-url=git://codeaurora.org/tools/repo.git --repo-branch=caf-stable
	repo sync -cj16 -q --no-tags -f --no-clone-bundle
	if [ $? -eq 0 ]
	then
		echo -e "$GREEN Downloading done..$ENDCOLOR"
	else
		echo -e "$RED!!!Error Downloading code!!!$ENDCOLOR"
	fi
else
	echo "repo tool problem, make sure you have setup your build environment"
	echo "1) http://source.android.com/source/initializing.html"
	echo "2) http://source.android.com/source/downloading.html (Installing Repo Section Only)"
	exit -1
fi
}

#copy Source Locally
copy_source()
{
	cd -
	cp -rf $BUILDSRC/./ $BUILDROOT/
	cd -
}

# Function to apply Quectel patches to CAF code
apply_android_patches()
{

	echo "Applying patches ..."
	if [ ! -e $PATCH_DIR ]
	then
		echo -e "$RED $PATCH_DIR : Not Found $ENDCOLOR"
	fi
	cd $PATCH_DIR
	tar -xvf $QUECTEL_PATCH_FILE -C $BUILDROOT
}

# Function to download & Apply OS Files
download_apply_os_files()
{
	echo "Dowloading OS Files ..."

	if [ -e $PATCH_DIR_OS ]
	then
		rm -rf $PATCH_DIR_OS
	fi

	mkdir -p $PATCH_DIR_OS/$OS_FILES_PATH
	wget -t 0 $OS_FILES_LINK -O $PATCH_DIR_OS/$OS_FILES_NAME
	tar -xf $PATCH_DIR_OS/$OS_FILES_NAME -C $PATCH_DIR_OS/$OS_FILES_PATH $OS_FILES_LIST
	cp -rf $PATCH_DIR_OS/os/./ $BUILDROOT/
}

#Function to extract QDN patches
function extract_qdn_patches()
{
	cd $WORKDIR
	unzip $QDN_PATCH_FILENAME -d temp_folder
	cp -rf temp_folder/*/patches3 patches3
	rm -rf temp_folder
}

# Function to commit Default Changes
commit_initial_changes()
{
	echo commit initial changes
	git init
	git add .
	git commit -m InitialCommit -s
}

# Function to apply patches to CAF code(2nd Set)
apply_android_patches2()
{

	echo "Applying patches 2 ..." $1
	PATCH_DIR=$1
	if [ ! -e $PATCH_DIR ]
	then
		echo -e "$RED $PATCH_DIR : Not Found $ENDCOLOR"
    exit
	fi

	cd $PATCH_DIR
	patch_root_dir="$PATCH_DIR"
	android_patch_list=$( find . -type f -name '*.patch' | sed -r 's|/[^/]+$||' |sort -u) &&
	for android_patch in $android_patch_list; do
		android_project=$android_patch
		echo -e "$YELLOW  Performing InitialCommit on $android_project ... $ENDCOLOR"
		cd $BUILDROOT/$android_project
		if [ $? -ne 0 ]; then
			echo -e "$RED $android_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
			mkdir -p $BUILDROOT/$android_project
			cd $BUILDROOT/$android_project
		fi
		commit_initial_changes
		pwd
	done
	cd $PATCH_DIR
	patch_root_dir="$PATCH_DIR"
	android_patch_list=$(find . -type f -name "*.patch" | sort) &&
	for android_patch in $android_patch_list; do
		android_project=$(dirname $android_patch)
		echo -e "$YELLOW   applying patches on $android_project ... $ENDCOLOR"
		cd $BUILDROOT/$android_project
		if [ $? -ne 0 ]; then
			echo -e "$RED $android_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
			exit 1
		fi
		pwd
		echo $patch_root_dir
		echo $android_patch
		git am $patch_root_dir/$android_patch
	done
}

# Function to apply patches to CAF code(2nd Set)
build()
{
	cd $BUILDROOT
	source build/envsetup.sh
	lunch msm8909-userdebug
	make -j16
}

echo "Begin"

echo check_program tar repo git patch

#Updates Path for 3.5 inch LCD option use parameter as 3p5
update_paths $1

#Setup Folders
setup_folders

#1 Download code
download_CAF_CODE
# copy_source
# 2 Apply Patches from Quectel
apply_android_patches
#3 Apply OS Files
download_apply_os_files
#4 Apply Patches for Carrier Board
apply_android_patches2 $PATCH_DIR2
#5 Apply QDN Patches
extract_qdn_patches
apply_android_patches2 $PATCH_DIR3
#6 Build
build

echo "End"
