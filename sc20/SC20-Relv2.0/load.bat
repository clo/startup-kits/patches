@echo off
TITLE [LOAD] (%cd%)
SET /A ARGS_COUNT=0
SET adbcommand=adb
FOR %%A in (%*) DO SET /A ARGS_COUNT+=1
if %ARGS_COUNT% == 1 SET adbcommand=%adbcommand% -s %1
echo %adbcommand%
@echo on
%adbcommand% reboot bootloader
pause
fastboot flash aboot emmc_appsboot.mbn
fastboot flash boot boot.img
fastboot flash system system.img
fastboot flash userdata userdata.img
fastboot flash recovery recovery.img
fastboot flash persist persist.img
fastboot flash cache cache.img
pause
fastboot reboot

