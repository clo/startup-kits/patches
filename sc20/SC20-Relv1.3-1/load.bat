@echo off
SET /A ARGS_COUNT=0
SET adbcommand=adb
SET IMAGE_FOLDER=Images
TITLE [LOAD] (%cd%\%IMAGE_FOLDER%)
FOR %%A in (%*) DO SET /A ARGS_COUNT+=1
if %ARGS_COUNT% == 1 SET adbcommand=%adbcommand% -s %1
echo %adbcommand%
@echo on
%adbcommand% reboot bootloader
pause
fastboot flash aboot %IMAGE_FOLDER%\emmc_appsboot.mbn
fastboot flash boot %IMAGE_FOLDER%\boot.img
fastboot flash system %IMAGE_FOLDER%\system.img
fastboot flash userdata %IMAGE_FOLDER%\userdata.img
fastboot flash recovery %IMAGE_FOLDER%\recovery.img
fastboot flash persist %IMAGE_FOLDER%\persist.img
fastboot flash cache %IMAGE_FOLDER%\cache.img
pause
fastboot reboot

