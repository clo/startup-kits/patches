From fc44a668b232edc987e8354c40bd8c666e411c8e Mon Sep 17 00:00:00 2001
From: Stephanie Buser <sbuser@codeaurora.org>
Date: Sun, 1 Mar 2020 19:49:31 +0530
Subject: [PATCH 08/11] Charging Status Enablement

Change-Id: Ia36d86b8946a1bc75b694f0702fc5a74dca26561
---
 drivers/power/qpnp-linear-charger.c |  6 +++
 drivers/usb/phy/phy-msm-usb.c       | 90 ++++++++++++++++++++++++++++++++++---
 include/linux/usb/msm_hsusb.h       |  2 +
 3 files changed, 93 insertions(+), 5 deletions(-)

diff --git a/drivers/power/qpnp-linear-charger.c b/drivers/power/qpnp-linear-charger.c
index 172e782..3f70032 100644
--- a/drivers/power/qpnp-linear-charger.c
+++ b/drivers/power/qpnp-linear-charger.c
@@ -1301,6 +1301,9 @@ static int get_prop_batt_status(struct qpnp_lbc_chip *chip)
 	int rc;
 	u8 reg_val;
 
+	if (!get_prop_batt_present(chip))
+		return POWER_SUPPLY_STATUS_DISCHARGING;
+
 	if (qpnp_lbc_is_usb_chg_plugged_in(chip) && chip->chg_done)
 		return POWER_SUPPLY_STATUS_FULL;
 
@@ -2482,6 +2485,9 @@ static irqreturn_t qpnp_lbc_usbin_valid_irq_handler(int irq, void *_chip)
 	int usb_present;
 	unsigned long flags;
 
+	if (!get_prop_batt_present(chip))
+		return IRQ_HANDLED;
+
 	usb_present = qpnp_lbc_is_usb_chg_plugged_in(chip);
 	pr_debug("usbin-valid triggered: %d\n", usb_present);
 
diff --git a/drivers/usb/phy/phy-msm-usb.c b/drivers/usb/phy/phy-msm-usb.c
index 5424e7e..000e775 100644
--- a/drivers/usb/phy/phy-msm-usb.c
+++ b/drivers/usb/phy/phy-msm-usb.c
@@ -78,6 +78,7 @@
 
 #define USB_DEFAULT_SYSTEM_CLOCK 80000000	/* 80 MHz */
 
+#define CONFIG_DETECT_CABLE_ENABLE
 enum msm_otg_phy_reg_mode {
 	USB_PHY_REG_OFF,
 	USB_PHY_REG_ON,
@@ -1893,12 +1894,40 @@ static int msm_otg_notify_chg_type(struct msm_otg *motg)
 	return 0;
 }
 
+static bool is_battery_present(struct msm_otg *motg)
+{
+	union power_supply_propval ret = {0,};
+	int rc;
+
+	if (motg->batt_psy == NULL)
+		motg->batt_psy = power_supply_get_by_name("battery");
+	if (motg->batt_psy) {
+		/* if battery has been registered, use the present property */
+		rc = motg->batt_psy->get_property(motg->batt_psy,
+					POWER_SUPPLY_PROP_PRESENT, &ret);
+		if (rc) {
+			pr_debug("qeuctel--battery does not export present: %d\n", rc);
+			return true;
+		}
+		return ret.intval;
+	}
+
+	/* Default to false if the battery power supply is not registered. */
+	pr_debug("quectel--battery power supply is not registered\n");
+	return false;
+}
+
 static int msm_otg_notify_power_supply(struct msm_otg *motg, unsigned mA)
 {
 	if (!psy) {
 		dev_dbg(motg->phy.dev, "no usb power supply registered\n");
 		goto psy_error;
 	}
+	if(!is_battery_present(motg)){
+		power_supply_set_online(psy, false);
+		goto out;
+	}
+
 
 	if (motg->cur_power == 0 && mA > 2) {
 		/* Enable charging */
@@ -1920,7 +1949,7 @@ static int msm_otg_notify_power_supply(struct msm_otg *motg, unsigned mA)
 		if (power_supply_set_current_limit(psy, 1000*mA))
 			goto psy_error;
 	}
-
+out:
 	power_supply_changed(psy);
 	return 0;
 
@@ -2907,6 +2936,9 @@ static const char *chg_to_string(enum usb_chg_type chg_type)
 	}
 }
 
+#ifdef CONFIG_DETECT_CABLE_ENABLE
+static int detect_count = 0;
+#endif
 #define MSM_CHG_DCD_TIMEOUT		(750 * HZ/1000) /* 750 msec */
 #define MSM_CHG_DCD_POLL_TIME		(50 * HZ/1000) /* 50 msec */
 #define MSM_CHG_PRIMARY_DET_TIME	(50 * HZ/1000) /* TVDPSRC_ON */
@@ -2920,7 +2952,7 @@ static void msm_chg_detect_work(struct work_struct *w)
 	u32 line_state, dm_vlgc;
 	unsigned long delay;
 
-	dev_dbg(phy->dev, "chg detection work\n");
+	dev_err(phy->dev, "chg detection work=%d\n",motg->chg_state);
 	msm_otg_dbg_log_event(phy, "CHG DETECTION WORK",
 			motg->chg_state, phy->state);
 
@@ -2976,6 +3008,9 @@ static void msm_chg_detect_work(struct work_struct *w)
 		} else {
 			delay = MSM_CHG_DCD_POLL_TIME;
 		}
+		#ifdef CONFIG_DETECT_CABLE_ENABLE
+		detect_count++;
+		#endif
 		break;
 	case USB_CHG_STATE_DCD_DONE:
 		vout = msm_chg_check_primary_det(motg);
@@ -3011,7 +3046,10 @@ static void msm_chg_detect_work(struct work_struct *w)
 				motg->chg_type = USB_FLOATED_CHARGER;
 			else
 				motg->chg_type = USB_SDP_CHARGER;
-
+			#ifdef CONFIG_DETECT_CABLE_ENABLE
+			if(detect_count > 5)
+				motg->chg_type = USB_DCP_CHARGER;
+			#endif
 			motg->chg_state = USB_CHG_STATE_DETECTED;
 			delay = 0;
 		}
@@ -3049,10 +3087,17 @@ static void msm_chg_detect_work(struct work_struct *w)
 		msm_chg_enable_aca_intr(motg);
 
 		/* Enable VDP_SRC in case of DCP charger */
+		#ifdef CONFIG_DETECT_CABLE_ENABLE
+		if ((motg->chg_type == USB_DCP_CHARGER) && (detect_count < 5))
+		{
+			pr_info("yxw write dcp ulpi!!!\n");
+			ulpi_write(phy, 0x2, 0x85);
+		}
+		#else
 		if (motg->chg_type == USB_DCP_CHARGER)
 			ulpi_write(phy, 0x2, 0x85);
-
-		dev_dbg(phy->dev, "chg_type = %s\n",
+		#endif
+		dev_err(phy->dev, "chg_type = %s\n",
 			chg_to_string(motg->chg_type));
 		msm_otg_dbg_log_event(phy, "CHG WORK: CHG_TYPE",
 				motg->chg_type, motg->inputs);
@@ -3218,6 +3263,9 @@ do_wait:
 	}
 }
 
+#ifdef CONFIG_DETECT_CABLE_ENABLE
+static bool vbus_online = false;
+#endif
 static void msm_otg_sm_work(struct work_struct *w)
 {
 	struct msm_otg *motg = container_of(w, struct msm_otg, sm_work);
@@ -3257,6 +3305,14 @@ static void msm_otg_sm_work(struct work_struct *w)
 		}
 		/* FALL THROUGH */
 	case OTG_STATE_B_IDLE:
+		#ifdef CONFIG_DETECT_CABLE_ENABLE
+		pr_info("yxw vbus online=%d\n",vbus_online);
+		if((vbus_online == 1)&& (!test_bit(B_SESS_VLD, &motg->inputs)))
+		{
+			pr_info("yxw OTG_STATE_B_IDLE set  B_SESS_VLD\n");
+			set_bit(B_SESS_VLD, &motg->inputs);
+		}
+		#endif
 		if (test_bit(MHL, &motg->inputs)) {
 			/* allow LPM */
 			msm_otg_dbg_log_event(&motg->phy, "PM RUNTIME: MHL PUT",
@@ -3278,6 +3334,7 @@ static void msm_otg_sm_work(struct work_struct *w)
 			work = 1;
 		} else if (test_bit(B_SESS_VLD, &motg->inputs)) {
 			pr_debug("b_sess_vld\n");
+			pr_info("yxw chg_state=%d\n",motg->chg_state);
 			msm_otg_dbg_log_event(&motg->phy, "B_SESS_VLD",
 					motg->inputs, otg->phy->state);
 			switch (motg->chg_state) {
@@ -3291,9 +3348,11 @@ static void msm_otg_sm_work(struct work_struct *w)
 				case USB_PROPRIETARY_CHARGER:
 					msm_otg_notify_charger(motg,
 							IDEV_CHG_MAX);
+					#ifndef CONFIG_DETECT_CABLE_ENABLE
 					otg->phy->state =
 						OTG_STATE_B_CHARGER;
 					work = 0;
+					#endif
 					msm_otg_dbg_log_event(&motg->phy,
 					"PM RUNTIME: PROPCHG PUT",
 					get_pm_runtime_counter(otg->phy->dev),
@@ -3695,6 +3754,9 @@ static void msm_otg_sm_work(struct work_struct *w)
 					A_WAIT_BCON);
 
 			/* Clear BSV in host mode */
+			#ifdef CONFIG_DETECT_CABLE_ENABLE
+			if(!vbus_online)
+			#endif
 			clear_bit(B_SESS_VLD, &motg->inputs);
 			msm_otg_start_host(otg, 1);
 			msm_chg_enable_aca_det(motg);
@@ -4167,12 +4229,19 @@ static void msm_otg_set_vbus_state(int online)
 
 	if (online) {
 		pr_debug("PMIC: BSV set\n");
+		#ifdef CONFIG_DETECT_CABLE_ENABLE
+		vbus_online = true;
+		#endif
 		msm_otg_dbg_log_event(&motg->phy, "PMIC: BSV SET",
 				init, motg->inputs);
 		if (test_and_set_bit(B_SESS_VLD, &motg->inputs) && init)
 			return;
 	} else {
 		pr_debug("PMIC: BSV clear\n");
+		#ifdef CONFIG_DETECT_CABLE_ENABLE
+		vbus_online = false;
+		detect_count = 0;
+		#endif
 		msm_otg_dbg_log_event(&motg->phy, "PMIC: BSV CLEAR",
 				init, motg->inputs);
 		if (!test_and_clear_bit(B_SESS_VLD, &motg->inputs) && init)
@@ -4186,6 +4255,17 @@ static void msm_otg_set_vbus_state(int online)
 		 * completion in UNDEFINED state.  Process
 		 * the initial VBUS event in ID_GND state.
 		 */
+		 #ifdef CONFIG_DETECT_CABLE_ENABLE
+		 if(online){
+			motg->chg_state = USB_CHG_STATE_DETECTED;
+			motg->chg_type = USB_DCP_CHARGER;
+			msm_otg_notify_charger(motg,IDEV_CHG_MAX);
+		}else{
+			motg->chg_state = USB_CHG_STATE_UNDEFINED;
+			motg->chg_type = USB_INVALID_CHARGER;
+			msm_otg_notify_charger(motg,0);
+		}
+		#endif
 		if (init)
 			return;
 	}
diff --git a/include/linux/usb/msm_hsusb.h b/include/linux/usb/msm_hsusb.h
index 0106722..5722e0f 100644
--- a/include/linux/usb/msm_hsusb.h
+++ b/include/linux/usb/msm_hsusb.h
@@ -433,9 +433,11 @@ struct msm_otg_platform_data {
 struct msm_otg {
 	struct usb_phy phy;
 	struct msm_otg_platform_data *pdata;
+	struct power_supply		*batt_psy;
 	int irq;
 	int async_irq;
 	int phy_irq;
+	int chg_gpio;
 	struct clk *xo_clk;
 	struct clk *pclk;
 	struct clk *core_clk;
-- 
2.7.4

